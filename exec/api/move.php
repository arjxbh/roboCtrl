<?php
if( !isset($_GET['arg']) || !isset($_GET['action']) ){
	// return error status
}else{
	require_once('head.php');
	require_once($SERVO_CFG);

	$movement = new move($QUEUE_PATH);
	if($servoList[$_GET['action']]){
	    $movement->enqueueIteravtive($_GET['action'],$_GET['arg']);
	}else{
		log::write('move api','animating action'.$_GET['action']);	
		$roboAction = new animate($QUEUE_PATH);
		$roboAction->$_GET['action']();
	}

	// api response
	echo 'moving - '.$_GET['action'].' '.$_GET['arg'];
}
