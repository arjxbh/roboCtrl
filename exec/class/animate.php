<?php

/**
* Class animate is used to macro scripted actions that the robot can perform
* and write them to the queue
**/
class animate extends robot {
	public function armsDown(){
		$movement = new move($this->queuePath);
		$makeMoves = array(
			'leftShoulderPan' => 660,
			'leftShoulderTilt' => 270,
			'leftElbow' => 200,
			'leftWrist' => 550,
			'rightShoulderPan' => 130,
			'rightShoulderTilt' => 560,
			'rightElbow' => 310,
			'rightWrist' => 330
		);
		$movement->enqueueAbsolute($makeMoves);

		$this->neutralPosition();
	}

	public function centerHead(){
		$movement = new move($this->queuePath);
		$makeMoves = array(
			'neckPan' => 380,
			'neckTilt' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
	}

	/**
	* Function - startUp - used to perform robot start up animitronics + systems test
	* also clears out the queue so that no residual actions are performed in case of crash
	**/
	public function startUp(){
		$movement = new move($this->queuePath);
		$makeMoves = array(
			'leftShoulderPan' => 10,
			'rightShoulderPan'=> 10,
			'leftElbow' => 160,
			'rightElbow' => 270
		);
		$movement->enqueueAbsolute($makeMoves);

		$expression = new express($this->queuePath);
		$expression->enqueue('face','blink');
		sleep(0.1);

		$expression->enqueue('body','theaterChaseWhite');
		sleep(0.1);

		$expression->enqueue('body','clear');
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 500,
			'neckPan' => 450
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 390
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'neckPan' => 240
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'neckPan' => 590
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckPan' => 390
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'leftShoulderPan' => 500,
			'rightShoulderPan'=> 300,
			'leftShoulderTilt' => 350,
			'rightShoulderTilt' => 500
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'leftElbow' => 400,
			'rightElbow'=> 500,
			'neckTilt' => 470
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.7);

		$makeMoves = array(
			'leftHand' => 280,
			'leftHandRotate' => 10
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'rightHand' => 170,
			'rightHandRotate' => 10
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'leftHand' => 550,
			'leftHandRotate' => 650
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'rightHand' => 280,
			'rightHandRotate' => 600
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 270,
			'leftElbow' => 220,
			'leftWrist' => 540,
			'leftHandRotate' => 300,
			'leftHand' => 300,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 557,
			'rightElbow' => 320,
			'rightWrist' => 290,
			'rightHandRotate' => 300,
			'rightHand' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 380,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$speech = new talk($this->queuePath);
		$speech->enqueue('speak','hello my name is henry');

		$this->armsDown();
		$this->centerhead();
	}

	/**
	* Function - get bored - function to be performed when robot hasn't done anything in a while
	* should occur prior to shutdown event for the purpose of saving battery
	**/
	public function getBored(){

	}

	/**
	*	.dP"Y8 88  88 88   88 888888 8888b.   dP"Yb  Yb        dP 88b 88
	*	`Ybo." 88  88 88   88   88    8I  Yb dP   Yb  Yb  db  dP  88Yb88
	*	o.`Y8b 888888 Y8   8P   88    8I  dY Yb   dP   YbdPYbdP   88 Y88
	*	8bodP' 88  88 `YbodP'   88   8888Y"   YbodP     YP  YP    88  Y8
	**/
	public function shutdown(){
		$expression = new express($this->queuePath);
		$expression->enqueue('face','clear');
		sleep(0.1);

		$movement = new move($this->queuePath);

		$makeMoves = array(
			'neckTilt' => 450,
			'neckPan' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$this->neutralPosition();
		sleep(2);

		$turnOff = new system($this->queuePath);
		$turnOff->enqueue('shutdown','shutdown -h now');
	}

	/**
	*		88""Yb 888888 88""Yb  dP"Yb   dP"Yb  888888
	*		88__dP 88__   88__dP dP   Yb dP   Yb   88
	*		88"Yb  88""   88""Yb Yb   dP Yb   dP   88
	*		88  Yb 888888 88oodP  YbodP   YbodP    88
	**/
	public function reboot(){
		$expression = new express($this->queuePath);
		$expression->enqueue('face','clear');
		sleep(0.1);

		$movement = new move($this->queuePath);

		$makeMoves = array(
			'neckTilt' => 450,
			'neckPan' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$this->neutralPosition();
		sleep(2);

		$turnOff = new system($this->queuePath);
		$turnOff->enqueue('reboot','reboot');
	}

	/**
	* Function - get rings - animatronic where robot will extend arms to get the ring box
	* after a certain number of seconds, the robot will pull arms back into the carry ring position
	*
	*	88""Yb 888888    db     dP""b8 88  88     88""Yb 88 88b 88  dP""b8 .dP"Y8
	*	88__dP 88__     dPYb   dP   `" 88  88     88__dP 88 88Yb88 dP   `" `Ybo."
	*	88"Yb  88""    dP__Yb  Yb      888888     88"Yb  88 88 Y88 Yb  "88 o.`Y8b
	*	88  Yb 888888 dP""""Yb  YboodP 88  88     88  Yb 88 88  Y8  YboodP 8bodP'
	**/
	public function reachRings(){
		$movement = new move($this->queuePath);

		$makeMoves = array(
			'neckTilt' => 300,
			'neckPan' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'leftShoulderPan' => 500,
			'rightShoulderPan'=> 300,
			'leftShoulderTilt' => 270,
			'rightShoulderTilt' => 560,
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'leftElbow' => 400,
			'rightElbow' => 500
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'leftWrist' => 500,
			'rightWrist' => 280,
			'leftHandRotate' => 150,
			'rightHandRotate' => 450,
			'leftHand' => 300,
			'rightHand' => 280
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$speech = new talk($this->queuePath);
		$speech->enqueue('speak','Please place the rings in my hands and wait for me to grab them.');
	}

	/**
	* Function - grabRings - function designed to run after reachRings
	* essentialy just closes the robots hand to a predetermined position
	* for the ring box.
	* After a short pause, the robot should look at the rings, and then
	* move to a position of low battery load for the duration of the
	* ring carry
	**/
	public function grabRings(){
		$movement = new move($this->queuePath);

		$makeMoves = array(
			'leftShoulderPan' => 500,
			'rightShoulderPan'=> 300,
			'leftShoulderTilt' => 270,
			'rightShoulderTilt' => 560,
			'leftElbow' => 400,
			'rightElbow' => 500,
			'leftWrist' => 500,
			'rightWrist' => 280,
			'leftHandRotate' => 150,
			'rightHandRotate' => 450,
			'leftHand' => 300,
			'rightHand' => 280
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'leftHand' => 350,
			'rightHand' => 225
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(2);

		$this->lookAround();

		$speech = new talk($this->queuePath);
		$speech->enqueue('speak','Thank you.');
		sleep(2);

		$makeMoves = array(
			'leftShoulderPan' => 550,
			'rightShoulderPan'=> 220,
			'leftShoulderTilt' => 270,
			'rightShoulderTilt' => 560,
			'leftElbow' => 400,
			'rightElbow' => 500,
			'leftWrist' => 500,
			'rightWrist' => 280,
			'leftHandRotate' => 150,
			'rightHandRotate' => 450
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$speech->enqueue('speak','I will be on my way.');
		sleep(0.1);
	}

	/**
	* Function - present rings - function where robot moves from a position
	* of low batery load, to a position where he is giving the rings to
	* someone
	**/
	public function presentRings(){
		$speech = new talk($this->queuePath);
		$speech->enqueue('speak','Here are the rings for the ceremony.');
		sleep(0.5);

		$movement = new move($this->queuePath);
		$makeMoves = array(
			'leftHand' => 340
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'leftShoulderPan' => 510,
			'leftShoulderTilt' => 270,
			'leftElbow' => 200,
			'leftWrist' => 550,
			'leftHandRotate' => 300,
			'leftHand' => 0,
			'neckTilt' => 340,
			'neckPan' => 310
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.3);


		$makeMoves = array(
			'leftShoulderTilt' => 270,
			'leftElbow' => 200,
			'leftHand' => 280
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(2);

		$makeMoves = array(
			'rightShoulderPan'=> 350,
			'rightShoulderTilt' => 560,
			'rightElbow' => 350,
			'rightWrist' => 450,
			'rightHandRotate' => 225,
			'neckTilt' => 340,
			'neckPan' => 310
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);
	}

	/**
	* Function - give rings - function where robot will give the rings
	* assumption here is that the robot is already carrying the rings
	**/
	public function releaseRings(){
		$movement = new move($this->queuePath);

		$speech = new talk($this->queuePath);
		$speech->enqueue('speak','here yo go.');
		sleep(0.7);

		$makeMoves = array(
			'rightHand' => 300
		);
		$movement->enqueueAbsolute($makeMoves);

		$this->neutralPosition();
		sleep(0.5);

		$makeMoves = array(
			'neckTilt' => 360,
			'neckPan' => 390
		);
		$movement->enqueueAbsolute($makeMoves);
	}

	/**
	* LOOK AROUND
	**/
	public function lookAround(){
		$movement = new move($this->queuePath);

		$makeMoves = array(
			'neckTilt' => 360,
			'neckPan' => 450
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(2);

		$makeMoves = array(
			'neckPan' => 240
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(2);

		$makeMoves = array(
			'neckPan' => 590
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(2);

		$makeMoves = array(
			'neckPan' => 390
		);
		$movement->enqueueAbsolute($makeMoves);
	}

	/**
	* neutralPosition
	**/
	public function neutralPosition(){
		$movement = new move($this->queuePath);
		$makeMoves = array(
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0,
			'neckTilt' => 0,
			'neckPan' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
	}

	/**
	* blink
	**/
	public function blink(){
		$expression = new express($this->queuePath);
		$expression->enqueue('face','blink');
	}

	/**
	* twist
	**/
	public function twist(){
		$expression = new express($this->queuePath);
		$expression->enqueue('face','smile');
		sleep(0.1);

		$movement = new move($this->queuePath);
		$makeMoves = array(
			'leftShoulderPan' => 460,
			'leftElbow' => 350,
			'leftWrist' => 550,
			'leftHand' => 330,
			'rightShoulderPan' => 310,
			'rightElbow' => 510,
			'rightWrist' => 330,
			'rightHand' => 280
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$twistMove = new walk($this->queuePath);

		for($i=0;$i<10;$i++){
			$twistMove->enqueue('spin',25);
			sleep(0.1);
			$twistMove->enqueue('spin',-25);
			sleep(0.1);
		}
		sleep(12);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 270,
			'leftElbow' => 220,
			'leftWrist' => 540,
			'leftHandRotate' => 300,
			'leftHand' => 300,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 557,
			'rightElbow' => 320,
			'rightWrist' => 290,
			'rightHandRotate' => 300,
			'rightHand' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 380,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$this->clear();
	}

	/**
	* spin DJ
	**/
	public function spinDJ(){
		$this->colorWipeFuscia();

		$expression = new express($this->queuePath);
		$expression->enqueue('face','smile');
		sleep(0.1);

		$movement = new move($this->queuePath);
		$makeMoves = array(
			'neckTilt' => 450,
			'leftShoulderPan' => 460,
			'leftElbow' => 350,
			'leftWrist' => 450,
			'leftHandRotate' => 360,
			'leftHand' => 330,
			'rightShoulderPan' => 310,
			'rightElbow' => 460,
			'rightWrist' => 200,
			'rightHand' => 280
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		for($i=0;$i<10;$i++){
			$makeMoves = array(
				'leftElbow' => 150,
				'leftWrist' => 260,
				'rightElbow' => 410,
				'rightWrist' => 180
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.1);

			$makeMoves = array(
				'leftElbow' => 200,
				'leftWrist' => 550,
				'rightElbow' => 260,
				'rightWrist' => 280
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.6);
		}
		sleep(0.5);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 270,
			'leftElbow' => 220,
			'leftWrist' => 540,
			'leftHandRotate' => 300,
			'leftHand' => 300,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 557,
			'rightElbow' => 320,
			'rightWrist' => 290,
			'rightHandRotate' => 300,
			'rightHand' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 380,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$this->clear();
	}

	/**
	*	Chop
	**/
	public function chop(){
		$this->colorWipeFuscia();

		$expression = new express($this->queuePath);
		$expression->enqueue('face','smile');
		sleep(0.1);

		$movement = new move($this->queuePath);
		$makeMoves = array(
			'leftShoulderPan' => 410,
			'leftShoulderTilt' => 520,
			'leftElbow' => 500,
			'rightShoulderPan' => 360,
			'rightShoulderTilt' => 350,
			'rightElbow' => 560,
			'rightWrist' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		for($i=0;$i<10;$i++){
			$makeMoves = array(
				'leftShoulderPan' => 410,
				'rightShoulderPan' => 210
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(1.5);
			$makeMoves = array(
				'leftShoulderPan' => 560,
				'rightShoulderPan' => 360
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(1.5);
		}
		$makeMovies = array(
			'rightShoulderPan' => 210
		);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 270,
			'leftElbow' => 220,
			'leftWrist' => 540,
			'leftHandRotate' => 300,
			'leftHand' => 300,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 557,
			'rightElbow' => 320,
			'rightWrist' => 290,
			'rightHandRotate' => 300,
			'rightHand' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 380,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$this->clear();
	}

	/**
	* Hands Up
	**/
	public function handsUp(){
		$this->colorWipeFuscia();

		$expression = new express($this->queuePath);
		$expression->enqueue('face','smile');
		sleep(0.1);

		$movement = new move($this->queuePath);
		$makeMoves = array(
			'neckTilt' => 250,
			'leftShoulderPan' => 650,
			'leftShoulderTilt' => 800,
			'leftElbow' => 200,
			'leftWrist' => 550,
			'leftHandRotate' => 360,
			'leftHand' => 330,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 150,
			'rightElbow' => 310,
			'rightWrist' => 330,
			'rightHandRotate' => 0,
			'rightHand' => 280
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		for($i=0;$i<10;$i++){
			$makeMoves = array(
				'leftElbow' => 150,
				'leftWrist' => 260,
				'rightElbow' => 410,
				'rightWrist' => 180
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.1);

			$makeMoves = array(
				'leftElbow' => 200,
				'leftWrist' => 550,
				'rightElbow' => 260,
				'rightWrist' => 280
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.1);
		}
		sleep(0.5);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 270,
			'leftElbow' => 220,
			'leftWrist' => 540,
			'leftHandRotate' => 300,
			'leftHand' => 300,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 557,
			'rightElbow' => 320,
			'rightWrist' => 290,
			'rightHandRotate' => 300,
			'rightHand' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 380,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$this->clear();
	}

	/**
	* higher
	**/
	public function higher(){
		$this->colorWipeFuscia();

		$expression = new express($this->queuePath);
		$expression->enqueue('face','smile');
		sleep(0.1);

		$movement = new move($this->queuePath);
		$makeMoves = array(
			'leftShoulderPan' => 410,
			'leftShoulderTilt' => 520,
			'leftElbow' => 500,
			'rightShoulderPan' => 360,
			'rightShoulderTilt' => 350,
			'rightElbow' => 560,
			'rightWrist' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'leftShoulderPan' => 560,
			'rightShoulderPan' => 210
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);

		$makeMoves = array(
			'leftShoulderPan' => 460,
			'rightShoulderPan' => 260
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);

		$makeMoves = array(
			'leftShoulderPan' => 410,
			'rightShoulderPan' => 310
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);
		$makeMoves = array(
			'leftShoulderPan' => 360,
			'rightShoulderPan' => 360
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);

		$makeMoves = array(
			'leftShoulderPan' => 310,
			'rightShoulderPan' => 410
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);
		$makeMoves = array(
			'leftShoulderPan' => 260,
			'rightShoulderPan' => 460
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);

		$makeMoves = array(
			'leftShoulderPan' => 210,
			'rightShoulderPan' => 510
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);

		$makeMoves = array(
			'leftShoulderPan' => 160,
			'rightShoulderPan' => 560
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);

		$makeMoves = array(
			'leftShoulderPan' => 560,
			'rightShoulderPan' => 210
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1.5);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 390,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 270,
			'leftElbow' => 220,
			'leftWrist' => 540,
			'leftHandRotate' => 300,
			'leftHand' => 300,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 557,
			'rightElbow' => 320,
			'rightWrist' => 290,
			'rightHandRotate' => 300,
			'rightHand' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.1);

		$makeMoves = array(
			'neckTilt' => 380,
			'leftShoulderPan' => 0,
			'leftShoulderTilt' => 0,
			'leftElbow' => 0,
			'leftWrist' => 0,
			'leftHandRotate' => 0,
			'leftHand' => 0,
			'rightShoulderPan' => 0,
			'rightShoulderTilt' => 0,
			'rightElbow' => 0,
			'rightWrist' => 0,
			'rightHandRotate' => 0,
			'rightHand' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$this->clear();
	}

	/**
	* robot dance
	**/
	public function robotDance(){
		$this->colorWipeFuscia();

		$expression = new express($this->queuePath);
		//$expression->enqueue('face','smile');
		sleep(0.1);

		// look right
		$movement = new move($this->queuePath);
		$makeMoves = array(
			'neckPan' => 140,
			'neckTilt' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		// spin right
		$twistMove = new walk($this->queuePath);
		$twistMove->enqueue('spin',-90);

		// rotate head back to match spin
		$makeMoves = array(
			'neckPan' => 200
		);
		$movement->enqueueAbsolute($makeMoves);
		$makeMoves = array(
			'neckPan' => 250
		);
		$movement->enqueueAbsolute($makeMoves);
		$makeMoves = array(
			'neckPan' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		$makeMoves = array(
			'neckPan' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'leftShoulderPan' => 410,
			'leftShoulderTilt' => 520,
			'leftElbow' => 500
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'rightShoulderPan' => 460,
			'rightShoulderTilt' => 350,
			'rightElbow' => 560,
			'rightWrist' => 300
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'leftShoulderPan' => 660,
			'leftShoulderTilt' => 270,
			'leftElbow' => 200
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$makeMoves = array(
			'neckTilt' => 450,
			'rightShoulderPan' => 150,
			'rightShoulderTilt' => 350,
			'rightElbow' => 560,
			'rightWrist' => 0
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'neckTilt' => 0,
			'rightElbow' => 510,
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'rightElbow' => 610,
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'rightElbow' => 510,
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'rightElbow' => 610,
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'rightElbow' => 510,
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$makeMoves = array(
			'rightElbow' => 560,
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		$this->neutralPosition();
		sleep(2);

		$makeMoves = array(
			'neckPan' => 630,
			'neckTilt' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(1);

		$twistMove = new walk($this->queuePath);
		$twistMove->enqueue('spin',90);

		$makeMoves = array(
			'neckPan' => 550
		);
		$movement->enqueueAbsolute($makeMoves);
		$makeMoves = array(
			'neckPan' => 500
		);
		$movement->enqueueAbsolute($makeMoves);
		$makeMoves = array(
			'neckPan' => 450
		);
		$movement->enqueueAbsolute($makeMoves);
		$makeMoves = array(
			'neckPan' => 380
		);
		$movement->enqueueAbsolute($makeMoves);
		sleep(0.5);

		for($i=0; $i<2; $i++){
			$makeMoves = array(
				'neckPan' => 140,
				'neckTilt' => 380,
				'rightShoulderTilt' => 350,
				'rightElbow' => 260,
				'rightWrist' => 330,
				'rightHandRotate' => 10,
				'rightHand' => 160
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'neckPan' => 0,
				'rightHand' => 300
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'neckPan' => 0,
				'rightHand' => 0,
				'rightWrist' => 100
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'rightWrist' => 300,
				'rightHandRotate' => 0,
				'rightElbow' => 560
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'rightElbow' => 0,
				'rightWrist' => 0,
				'rightShoulderTilt' => 560
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'rightWrist' => 0,
				'rightShoulderTilt' => 0,
				'neckPan' => 380,
				'neckTilt' => 380
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'neckPan' => 630,
				'neckTilt' => 380
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftShoulderTilt' => 570,
				'neckTilt' => 0
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftElbow' => 200
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftWrist' => 550
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftHandRotate' => 110
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftHand' => 420
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftHand' => 280
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftHand' => 0,
				'leftHandRotate' => 0,
				'leftWrist' => 300
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftWrist' => 550,
				'leftElbow' => 450
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$makeMoves = array(
				'leftWrist' => 0,
				'leftElbow' => 0,
				'leftShoulderTilt' => 270
			);
			$movement->enqueueAbsolute($makeMoves);
			sleep(0.5);

			$this->armsDown();

			//$this->neutralPosition();

			$makeMoves = array(
				'neckPan' => 380,
				'neckTilt' => 380
			);
			$movement->enqueueAbsolute($makeMoves);
		}

		$this->clear();
	}

	public function clear(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','clear');
	}

	public function colorWipeRed(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','colorWipeRed');
	}

	public function colorWipeBlue(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','colorWipeBlue');
	}

	public function colorWipeGreen(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','colorWipeGreen');
	}

	public function colorWipeFuscia(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','colorWipeFuscia');
	}

	public function theaterChaseWhite(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','theaterChaseWhite');
	}

	public function theaterChaseRed(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','theaterChaseRed');
	}

	public function theaterChaseBlue(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','theaterChaseBlue');
	}

	public function rainbow(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','rainbow');
	}

	public function rainbowCycle(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','rainbowCycle');
	}

	public function theaterChaseRainbow(){
		$expression = new express($this->queuePath);
		$expression->enqueue('body','theaterChaseRainbow');
	}
}
