<?php
/**
* Class express is used to control the lighting system
**/
class express extends robot {
	public function enqueue($bodyPart, $action){
		log::write('info express',$bodyPart.' - '.$action);

		$fileName = $this->queuePath.'/'.queue::getMicrotime().'.express.'.$bodyPart.'.q';

		touch($fileName);
		$queueFile = fopen($fileName, 'w');
		fwrite($queueFile, $action);
		fclose($queueFile);
	}

	public static function setLights($bodyPart, $action, $modelPath, $GPIO){
		// select GPIO to set relay, make GPIO "out" so it pushes electricity instead of reading
		$result = exec('/usr/local/bin/gpio mode '.$GPIO.' out');
		if($result){log::write('info(gpio mode)',$result);}
		switch($bodyPart){
			case 'face':
				// turning GPIO off is wired to face
				exec('/usr/local/bin/gpio write '.$GPIO.' 0');
				if($result){log::write('info(gpio write)',$result);}
				exec( $modelPath.'/neopixel/face.py '.$action );
				if($result){log::write('info(neopixel face)',$result);}
				break;
			case 'body':
				// turning GPIO on is wired to body
				exec('/usr/local/bin/gpio write '.$GPIO.' 1');
				exec( $modelPath.'/neopixel/body.py '.$action );
				break;
		}
	}
}
