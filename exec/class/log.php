<?php

/**
* Class log is used to manage logging of events
**/
class log extends robot{
  public static function write($level, $message){
	$LOG_PATH = '/home/pi/roboCtrl/log';
    $fileName = $LOG_PATH.'/error.log';

    //TODO: figure out how to open file for editing
    // instead of overwriting the file

    $errorLogFile = fopen($fileName, 'a');
    fwrite($errorLogFile, date("Y-m-d H:i:s").'|'.$level.'|'.$message."\n");
    fclose($errorLogFile);

    //exec('tail -n 1000 '.$errorLogFile.' > '.$errorLogFile);
  }
}
