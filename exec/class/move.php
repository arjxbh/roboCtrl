<?php
/**
* Class move is to issue commands to the servos and keep track of servo positions
**/
class move extends robot {
	/*
	public function __construct($QUEUE_PATH, $servoProps, $SERVO_POS){
		parent::__construct($QUEUE_PATH);
	}
	*/

	public function enqueueIteravtive($action, $arg){
		$fileName = $this->queuePath.'/'.queue::getMicrotime().'.move.'.$action.'.q';

		touch($fileName);
		$queueFile = fopen($fileName, 'w');
		fwrite($queueFile, $action."|iterate|".$arg);
		fclose($queueFile);
	}

	public function enqueueAbsolute($argObj){
		log::write('info move','enqueuing: '.$argObj);
		$fileName = $this->queuePath.'/'.queue::getMicrotime().'.move.'.$action.'.q';

		touch($fileName);
		$queueFile = fopen($fileName, 'w');

		foreach($argObj as $key => $value){
			fwrite($queueFile, $key."|set|".$value.PHP_EOL);
		}

		fclose($queueFile);
	}

	public static function runServos( $fileName, &$positionObj = null, $MODEL_PATH ){
		log::write('info move','running Servos'.var_dump($positionObj));
		// read file
		$instructions = file($fileName, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

		// look through lines, update position array
		foreach ($instructions as $line_num => $line) {
			log::write('move','reading '.$line.' from '.$fileName);
      			$field = explode("|", $line);
			switch($field[1]){
		        	case 'iterate':
								$current = $positionObj->getPosition($field[0]) + intval($field[2]);
          				break;
        			case 'set':
									$current = $field[2];
          				break;
      			}

			if( $current != 0 ){
				//verify that the instructions don't exceed the
				//limits of the physical servo only if 0 is not issued
				if( $current > $positionObj->getMax($field[0]) ){
					$current = $positionObj->getMax($field[0]);
				}
				if( $current < $positionObj->getMin($field[0]) ){
					$current = $positionObj->getMin($field[0]);
				}
			}

			//set position in object
			$positionObj->setPosition( $field[0], $current );
		}
		log::write('info move',$positionObj->getPositionString());
		exec( $MODEL_PATH.'/move.py '.$positionObj->getPositionString() );
	}
}
