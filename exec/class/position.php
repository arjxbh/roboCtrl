<?php
/**
* Class Position is used to track all the servos
**/

class position{
	private $positions = array();

	public function __construct(){
	}

	public function createServo($index, $name, $min, $max, $neutral){
		$this->positions[$name] = new servo($index, $name, $min, $max, $neutral);
	}

	public function setPosition($name, $pos){
		$this->positions[$name]->setPosition($pos);
	}

	public function getPosition($name){
		return $this->positions[$name]->getPosition();
	}

	public function getMax($name){
		return $this->positions[$name]->getMax();
	}

	public function getMin($name){
		return $this->positions[$name]->getMin();
	}

	public function getNeutral($name){
		return $this->positions[$name]->getNeutral();
	}

	public function getAllPositions(){
		return $this->positions;
	}

	public function getPositionString(){
		$positionString = '';
		$positionStringArray = array();

		// grab all positions and store to an array sorted by index
		foreach($this->positions as $joint => $properties){
			//$positionString = $positionString.' '.$this->positions[$joint]->getPosition();
			$positionStringArray[$this->positions[$joint]->getIndex()]=$this->positions[$joint]->getPosition();
		}

		// write to position string in the correct order from positionStringArray
		$positionTracker = 0;

		while( $positionTracker < 16 ){
			if( $positionStringArray[$positionTracker] ){
				$positionString = $positionString.' '.$positionStringArray[$positionTracker];
			}else{
				$positionString = $positionString.' 0';
			}
			$positionTracker++;
		}

		return $positionString;
	}
}
