<?php

/**
* Class QUEUE is used to read items from the queue and call the appropriate action
**/
class queue extends robot{
	private $queueRun;
	private $modelPath;
	private $queueType;

	public function __construct($QUEUE_PATH, $MODEL_PATH, $type=null){
		parent::__construct($QUEUE_PATH);
		$this->queueRun = true;
		$this->modelPath = $MODEL_PATH;
		$this->queueType = $type;
	}

	public function getRunState(){
		return $this->queueRun;
	}

	/**
	*	Delete a queue of a given type
	**/
	public function eraseQueue($type){
		$queue = scandir($this->queuePath);

		foreach( $queue as $file ){
			$queueParts = explode('.',$file);
			if( isset($queueParts[3]) ){
				if( $queueParts[3] == 'q' && $queueParts[1] == $type ){
					unlink($this->queuePath.'/'.$file);
				}
			}
		}
	}

	public static function stopQueue($QUEUE_PATH){
		$fileName = $QUEUE_PATH.'/'.queue::getMicrotime().'.stop.stop.q';
		touch($fileName);
	}

	public function consumeQueue( $fileName, &$positionObj = null ){
		$queueParts = explode('.',$fileName);
		switch( $queueParts[1] ){
			case 'system':
				$instructions = file($this->queuePath.'/'.$fileName.'.p');
				$doThis = $queueParts[2];
				system::runSystemCommand($instructions[0]);
				chmod($this->queuePath.'/'.$fileName.'.p', 0777);
				unlink($this->queuePath.'/'.$fileName.'.p');
				break;
			case 'talk':
				$instructions = file($this->queuePath.'/'.$fileName.'.p');
				$doThis = $queueParts[2];
				talk::$doThis($instructions[0]);
				chmod($this->queuePath.'/'.$fileName.'.p', 0777);
				unlink($this->queuePath.'/'.$fileName.'.p');
				break;
			
			case 'walk':
				// read queue
				$instructions = file($this->queuePath.'/'.$fileName.'.p');
				switch($queueParts[2]){
					case 'straight':
						walk::walkStraight($instructions[0],$this->modelPath);
						break;
					case 'spin':
						walk::walkSpin($instructions[0],$this->modelPath);
						break;
				}

				// delete queue
				chmod($this->queuePath.'/'.$fileName.'.p', 0777);
				unlink($this->queuePath.'/'.$fileName.'.p');
				break;

			case 'move':
				move::runServos( $this->queuePath.'/'.$fileName.'.p' , $positionObj , $this->modelPath);
				chmod($this->queuePath.'/'.$fileName.'.p', 0777);
				unlink($this->queuePath.'/'.$fileName.'.p');
				break;

			case 'express':
				$instructions = file($this->queuePath.'/'.$fileName.'.p');
				print_r($instructions);
				express::setLights($queueParts[2],$instructions[0],$this->modelPath,0);
				chmod($this->queuePath.'/'.$fileName.'.p', 0777);
				unlink($this->queuePath.'/'.$fileName.'.p');

				break;

			case 'stop':
				echo 'stop running queue';
				echo "\n";

				$this->eraseQueue($this->queueType);
				$this->queueRun = false;
				break;

			default:
				echo 'ERROR: unknown action! log to file!';
				echo "\n";
				break;
		}
	}

	/**
	* Function readQueue() - reads the queue and do the appropriate action
	**/
	public function readQueue( $queueType, &$positionObj = null ){
// 		foreach (glob("*.txt") as $filename) {
//     echo "$filename size " . filesize($filename) . "\n";
// }

		//$queue = opendir($this->queuePath);

		//while( $file = readdir($queue) ){
		foreach( glob("*.q") as $file ){
			$queueParts = explode('.',$file);
			if( isset($queueParts[3]) ){
				if( $queueParts[1] == $queueType || $queueParts[1] == 'stop' ){
					switch( $queueParts[1] ){
						case 'system':
							echo 'found system';
							echo '\n';
							break;
						case 'talk':
							echo 'found talk';
							echo '\n';
							break;
						case 'walk':
							echo 'found walk';
							echo "\n";

							// read queue
							$instructions = file($this->queuePath.'/'.$file);
							print_r($instructions);
							switch($queueParts[2]){
								case 'straight':
									walk::walkStraight($instructions[0],$this->modelPath);
									break;
								case 'spin':
									walk::walkSpin($instructions[0],$this->modelPath);
									break;
							}

							// delete queue
							unlink($this->queuePath.'/'.$file);

							break;

						case 'move':
							echo 'found move - write to servo position file';
							echo "\n";
							echo 'once file is written execute entire file, so all positions new and old are issued.';
							echo "\n";
							break;

						case 'express':
							echo 'found express';
							echo "\n";

							// read queue
							$instructions = file($this->queuePath.'/'.$file);
							print_r($instructions);
							express::setLights($queueParts[2],$instructions[1],$this->modelPath);

							// delete queue
							unlink($this->queuePath.'/'.$file);

							break;

						case 'stop':
							echo 'found stop - ignore additional message, consume queue and do not act';
							echo 'stop running queue';
							echo "\n";

							// delete all files except for stop
							$stopQueue = scandir($this->queuePath);
							foreach( $stopQueue as $queueFile ){
								$fileParts = explode('.',$queueFile);
								if( isset($fileParts[3]) ){
									if( $fileParts[1] != 'stop' ) {
											unlink($this->queuePath.'/'.$queueFile);
									}
								}
							}

							$this->queueRun = false;

							break;

						default:
							echo 'ERROR: unknown action! log to file!';
							echo "\n";
							break;
					}

					//closedir($queue);
				}
			}
		}
	}
}
