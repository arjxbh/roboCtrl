<?php

/**
* Class queueThreads used for multithreading
**/
class queueThreads extends Thread{
  private $queueType;
  private $queuePath;
  private $modelPath;

  public function __construct($type, $queuePath, $modelPath){
    $this->queuePath = $queuePath;
    $this->modelPath = $modelPath;
    $this->queueType = $type;
  }

  public function run(){
    $roboQueue = new queue($this->queuePath, $this->modelPath);

    while($roboQueue->getRunState()){
      $roboQueue->readQueue($this->queueType);
    }
  }
}
