<?php
/**
* Class robot is the base class used for the robot
* It keeps track of the queue path and generated Microtime
**/

class robot {
	protected $queuePath;

	/**
	* Constructor - sets up the object with the property queue path
	*@PARAM - $QUEUE_PATH - string - reads in the absolute path to the queue directory
	**/
	public function __construct($QUEUE_PATH){
		$this->queuePath = $QUEUE_PATH;
	}

	public static function getMicrotime()
	{
	    	list($usec, $sec) = explode(" ", microtime());
    		return ceil( ( $usec * 1000 )+ ( $sec * 1000 ) );
	}
}
