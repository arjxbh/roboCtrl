<?php
/**
* Class Position is used to track an individual servo
**/

class servo {
	private $index;
	private $min;
	private $max;
	private $neutral;
	private $name;
	private $position;

	public function __construct($idx, $name, $min, $max, $neutral){
		$this->index = $idx;
		$this->name = $name;
		$this->min = $min;
		$this->max = $max;
		$this->neutral = $neutral;
		$this->position = $neutral;
	}

	public function getMin(){
		return $this->min;
	}

	public function getMax(){
		return $this->max;
	}

	public function getNeutral(){
		return $this->neutral;
	}

	public function getPosition(){
		return $this->position;
	}

	public function getIndex(){
		return $this->index;
	}

	public function setPosition($pos){
		$this->position = $pos;
	}
}
