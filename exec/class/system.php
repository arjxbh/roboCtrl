<?php

/**
* Class system is used for super user level system commands
**/
class system extends robot {
	public function enqueue($action, $arg){
		log::write('info system','enqueuing: '+$action+","+$arg);
		$fileName = $this->queuePath.'/'.queue::getMicrotime().'.system.'.$action.'.q';

		touch($fileName);
		$queueFile = fopen($fileName, 'w');
		fwrite($queueFile, $arg);
		fclose($queueFile);
	}

	public static function runSystemCommand($command){
		exec($command);
	}
}
