<?php

/**
* Class talk is used to issue talk queue and speak
**/
class talk extends robot {
	public function enqueue($action, $arg){
		log::write('info talk','enqueuing: '+$action+","+$arg);
		$fileName = $this->queuePath.'/'.queue::getMicrotime().'.talk.'.$action.'.q';

		touch($fileName);
		$queueFile = fopen($fileName, 'w');
		fwrite($queueFile, $arg);
		fclose($queueFile);
	}

	public static function volume($level){
		exec('amixer cset numid=6 -- '.$level);
	}

	public static function speak($text){
		$talkTime = ceil(strlen($text) / 20);
		// fix this hardcoded value later!!
		$talkAnim = new express('/home/pi/roboCtrl/queue');
		$i=0;
		while($i<$talkTime){
			$talkAnim->enqueue('face','talk');
			sleep(0.01);
			$i++;
		}
		exec('echo "'.$text.'" | festival --tts');
	}

	public static function sing($file){
		// command to play music
	}

	public function greeting(){
		$currentHour=date('H');

		$greetingTime = 'morningGreeting';
		if( $currentHour > 12 )$greetingTime = 'afternoonGreeting';
		if( $currentHour > 5 )$greetingTime = 'eveningGreeting';

		$greetingType = rand(0,1);

		if( $greetingType == 0 ){
			$this->$greetingTime();
		}else{
			$this->genericGreeting();
		}
	}

	public function genericGreeting(){
		$randSpeak = rand(0,3);

		switch($randSpeak){
			case 0:
				talk::speak('Hello 0');
				break;
			case 1:
				talk::speak('Hello 1');
				break;
			case 2:
				talk::speak('Hello 2');
				break;
			case 3:
				talk::speak('Hello 3');
				break;
		}
	}

	public function morningGreeting(){
		talk::speak('good morning');
	}

	public function afternoonGreeting(){
		talk::speak('good afternoon');
	}

	public function eveningGreeting(){
		talk::speak('good evening');
	}
}
