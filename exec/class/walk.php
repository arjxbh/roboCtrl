<?php

/**
* Class walk is used to react to the drivetrain commands
**/
class walk extends robot {
	public function enqueue($action, $arg){
		$fileName = $this->queuePath.'/'.queue::getMicrotime().'.walk.'.$action.'.q';

		touch($fileName);
		$queueFile = fopen($fileName, 'w');
		fwrite($queueFile, $arg);
		fclose($queueFile);
	}

	public static function walkStraight($distance, $modelPath){
		exec( $modelPath.'/driveStraight.py '.$distance );
	}

	public static function walkSpin($angle, $modelPath){
		exec( $modelPath.'/driveSpin.py '.$angle );
	}

	public static function walkStop(){
		exec( $modelPath.'/driveStop.py' );
	}
}
