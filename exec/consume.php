#!/usr/bin/php
<?php
require_once('./head.php');

//get queue type from command line argument
$queueType = $argv[1];

echo 'starting queue consumer for '.$queueType;

if($queueType == 'move'){
	require_once($SERVO_CFG);
	$servoPositions = new position();

	foreach($servoList as $name => $config){
		$servoPositions->createServo($config['index'], $name, $config['min'], $config['max'], $config['neutral']);
	}
}

$roboQueue = new queue($QUEUE_PATH, $MODEL_PATH, $queueType);
$roboQueue->eraseQueue($queueType);

$latestQueue = '';
$noQueue = 0;

while( $latestQueue != 'stop' ){
	$latestQueue = exec($BASE_DIR.'/exec/readDir.sh '.$queueType.' '.$QUEUE_PATH.' '.$BASE_DIR.' '.$LOG_FILE );
	// if the queue type is move and a stop is issued, perhaps 0 should be issued to all 16 channels

	if($latestQueue && $latestQueue != 'stop' && $latestQueue != 'NOQUEUE'){
		log::write('info '.$queueType, 'consuming queue '.$latestQueue.'.p');
		$queueRun = new queue($QUEUE_PATH, $MODEL_PATH, $queueType);
		$queueRun->consumeQueue($latestQueue, $servoPositions);
		$noQueue = 0;
	}
	if($latestQueue == 'NOQUEUE'){$noQueue++;}
	if($noQueue > 50 && $queueType == 'walk'){
		walk::walkStop();
	}
	sleep(0.01);
}
