#!/usr/bin/php
<?php
include('../cfg/robo_env');

function __autoload($classname) {
    $filename = "./class/". $classname .".php";
    include_once($filename);
}

// what is the purpose of this??  needs to be rewritten.
function queueAction($action,$part,$arg){
	global $QUEUE_PATH;
	touch($QUEUE_PATH.'/'.queue::getMicrotime().'.'.$action.'.'.$part.'.q');
	$queueFile = fopen($QUEUE_PATH.'/'.microtime_float().'.'.$action.'.'.$part.'.q', 'w');
	fwrite($queueFile, $arg);
	fclose($queueFile);
}

// valid actions should be walk, move, express
// walk is used to "walk" around using the drive system
// move is used to "move" any servos
// express is used to manipulate any of the lighting system

queueAction('walk','forward',1);
