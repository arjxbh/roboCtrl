<?php
require_once('../cfg/robo_env');

function __autoload($classname) {
    $filename = "./class/". $classname .".php";
    include_once($filename);
}