import RPi.GPIO as GPIO ## Import GPIO library
GPIO.setmode(GPIO.BOARD) ## Use board pin numbering
GPIO.setup(17, GPIO.OUT)
GPIO.output(17, True)
GPIO.cleanup()
