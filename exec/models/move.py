#!/usr/bin/python

from Adafruit_PWM_Servo_Driver import PWM
import time
import sys, getopt

# ===========================================================================
# Example Code
# ===========================================================================

# Initialise the PWM device using the default address
pwm = PWM(0x40)
# Note if you'd like more debug output you can instead run:
#pwm = PWM(0x40, debug=True)

servoMin = 150  # Min pulse length out of 4096
servoMax = 600  # Max pulse length out of 4096

def setServoPulse(channel, pulse):
  pulseLength = 1000000                   # 1,000,000 us per second
  pulseLength /= 60                       # 60 Hz
  print "%d us per period" % pulseLength
  pulseLength /= 4096                     # 12 bits of resolution
  print "%d us per bit" % pulseLength
  pulse *= 1000
  pulse /= pulseLength
  pwm.setPWM(channel, 0, pulse)

pwm.setPWMFreq(60)                        # Set frequency to 60 Hz

print '1'
print sys.argv[1]
print '2'
print sys.argv[2]
print '3'
print sys.argv[3]
print '4'
print sys.argv[4]
print '5'
print sys.argv[5]
print '6'
print sys.argv[6]
print '7'
print sys.argv[7]
print '8'
print sys.argv[8]
print '9'
print sys.argv[9]
print '10'
print sys.argv[10]
print '11'
print sys.argv[11]
print '12'
print sys.argv[12]
print '13'
print sys.argv[13]
print '14'
print sys.argv[14]
print '15'
print sys.argv[15]
print '16'
print sys.argv[16]


pwm.setPWM(0, 0, int(sys.argv[1]))
pwm.setPWM(1, 0, int(sys.argv[2]))
pwm.setPWM(2, 0, int(sys.argv[3]))
pwm.setPWM(3, 0, int(sys.argv[4]))
pwm.setPWM(4, 0, int(sys.argv[5]))
pwm.setPWM(5, 0, int(sys.argv[6]))
pwm.setPWM(6, 0, int(sys.argv[7]))
pwm.setPWM(7, 0, int(sys.argv[8]))
pwm.setPWM(8, 0, int(sys.argv[9]))
pwm.setPWM(9, 0, int(sys.argv[10]))
pwm.setPWM(10, 0, int(sys.argv[11]))
pwm.setPWM(11, 0, int(sys.argv[12]))
pwm.setPWM(12, 0, int(sys.argv[13]))
pwm.setPWM(13, 0, int(sys.argv[14]))
pwm.setPWM(14, 0, int(sys.argv[15]))
pwm.setPWM(15, 0, int(sys.argv[16]))
