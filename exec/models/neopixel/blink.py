# NeoPixel library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.
import time

from neopixel import *


# LED strip configuration:
LED_COUNT      = 32      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 32     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)

# Define functions which animate LEDs in various ways.
def blink(strip):
	min = 1
	max = 24 
	count = 0
	blinkSpeed = 200 

	#while (count < 24):
	#	strip.setPixelColor(count-1, Color( 0, 0, 0))
	#	strip.setPixelColor(count, Color( 255, 255, 255))
	#	strip.show()
	#	count = count + 1
	#	time.sleep(300/1000.0)

	while (count < max):
		strip.setPixelColor(count, Color( 255, 255, 255))
		count=count+1

	strip.setPixelColor(3, Color(0, 0, 0))
	strip.setPixelColor(4, Color(0, 0, 0))
	strip.setPixelColor(9, Color(0, 0, 0))
	strip.setPixelColor(10, Color(0, 0, 0))
	
	strip.setPixelColor(15, Color(0, 0, 0))
	strip.setPixelColor(16, Color(0, 0, 0))
	strip.setPixelColor(22, Color(0, 0, 0))
	strip.setPixelColor(21, Color(0, 0, 0))
	
	strip.show()
	
	time.sleep(1)

	strip.setPixelColor(3, Color(0, 0, 0))
	strip.setPixelColor(4, Color(0, 0, 0))
	strip.setPixelColor(9, Color(0, 0, 0))
	strip.setPixelColor(10, Color(0, 0, 0))
	
	strip.setPixelColor(15, Color(0, 0, 0))
	strip.setPixelColor(16, Color(0, 0, 0))
	strip.setPixelColor(22, Color(0, 0, 0))
	strip.setPixelColor(21, Color(0, 0, 0))
	
	strip.show()
	time.sleep(blinkSpeed/1000.0)
	
	strip.setPixelColor(2, Color(0, 0, 0))
	strip.setPixelColor(5, Color(0, 0, 0))
	strip.setPixelColor(8, Color(0, 0, 0))
	strip.setPixelColor(11, Color(0, 0, 0))
	
	strip.setPixelColor(14, Color(0, 0, 0))
	strip.setPixelColor(17, Color(0, 0, 0))
	strip.setPixelColor(20, Color(0, 0, 0))
	strip.setPixelColor(23, Color(0, 0, 0))
	
	strip.show()
	time.sleep(blinkSpeed/1000.0)
	
	strip.setPixelColor(0, Color(0, 0, 0))
	strip.setPixelColor(1, Color(0, 0, 0))
	strip.setPixelColor(6, Color(0, 0, 0))
	strip.setPixelColor(7, Color(0, 0, 0))
	strip.setPixelColor(12, Color(0, 0, 0))
	
	strip.setPixelColor(13, Color(0, 0, 0))
	strip.setPixelColor(18, Color(0, 0, 0))
	strip.setPixelColor(19, Color(0, 0, 0))
	strip.setPixelColor(24, Color(0, 0, 0))
	
	strip.show()
	time.sleep(blinkSpeed/1000.0)
	
	strip.setPixelColor(0, Color(255, 255, 255))
	strip.setPixelColor(1, Color(255, 255, 255))
	strip.setPixelColor(6, Color(255, 255, 255))
	strip.setPixelColor(7, Color(255, 255, 255))
	strip.setPixelColor(12, Color(255, 255, 255))
	
	strip.setPixelColor(13, Color(255, 255, 255))
	strip.setPixelColor(18, Color(255, 255, 255))
	strip.setPixelColor(19, Color(255, 255, 255))
	strip.setPixelColor(24, Color(255, 255, 255))
	
	strip.show()
	time.sleep(blinkSpeed/1000.0)

	strip.setPixelColor(2, Color(255, 255, 255))
	strip.setPixelColor(5, Color(255, 255, 255))
	strip.setPixelColor(8, Color(255, 255, 255))
	strip.setPixelColor(11, Color(255, 255, 255))
	
	strip.setPixelColor(14, Color(255, 255, 255))
	strip.setPixelColor(17, Color(255, 255, 255))
	strip.setPixelColor(20, Color(255, 255, 255))
	strip.setPixelColor(23, Color(255, 255, 255))
	strip.show()
	time.sleep(blinkSpeed/1000.0)

	#strip.setPixelColor(3, Color(255, 255, 255))
	#strip.setPixelColor(4, Color(255, 255, 255))
	#strip.setPixelColor(9, Color(255, 255, 255))
	#strip.setPixelColor(10, Color(255, 255, 255))
	
	#strip.setPixelColor(15, Color(255, 255, 255))
	#strip.setPixelColor(16, Color(255, 255, 255))
	#strip.setPixelColor(22, Color(255, 255, 255))
	#strip.setPixelColor(21, Color(255, 255, 255))
	#strip.show()
	#time.sleep(blinkSpeed/1000.0)

	
# Main program logic follows:
if __name__ == '__main__':
	# Create NeoPixel object with appropriate configuration.
	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
	# Intialize the library (must be called once before other functions).
	strip.begin()

	print 'Press Ctrl-C to quit.'
	blink(strip)
