#!/usr/bin/env python
# coding: Latin-1

import time
import sys
import sys, getopt

from neopixel import *

# LED strip configuration:
LED_COUNT      = 40      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5    # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)

# Define functions which animate LEDs in various ways.
def neutralFace(strip):
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.show()

def smile(strip):
        strip.setPixelColor(0, Color(255, 255, 255))
        strip.setPixelColor(1, Color(255, 255, 255))
        strip.setPixelColor(6, Color(255, 255, 255))
        strip.setPixelColor(7, Color(255, 255, 255))
        strip.setPixelColor(12, Color(255, 255, 255))

        strip.setPixelColor(13, Color(255, 255, 255))
        strip.setPixelColor(18, Color(255, 255, 255))
        strip.setPixelColor(19, Color(255, 255, 255))
        strip.setPixelColor(23, Color(255, 255, 255))

        strip.setPixelColor(2, Color(255, 255, 255))
        strip.setPixelColor(5, Color(255, 255, 255))
        strip.setPixelColor(8, Color(255, 255, 255))
        strip.setPixelColor(11, Color(255, 255, 255))

        strip.setPixelColor(14, Color(255, 255, 255))
        strip.setPixelColor(17, Color(255, 255, 255))
        strip.setPixelColor(20, Color(255, 255, 255))
        strip.setPixelColor(23, Color(255, 255, 255))

	strip.setPixelColor(25, Color(0, 255, 255))
	strip.setPixelColor(30, Color(0, 255, 255))
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.show()

def frown(strip):
        strip.setPixelColor(0, Color(255, 255, 255))
        strip.setPixelColor(1, Color(255, 255, 255))
        strip.setPixelColor(6, Color(255, 255, 255))
        strip.setPixelColor(7, Color(255, 255, 255))
        strip.setPixelColor(12, Color(255, 255, 255))

        strip.setPixelColor(13, Color(255, 255, 255))
        strip.setPixelColor(18, Color(255, 255, 255))
        strip.setPixelColor(19, Color(255, 255, 255))
        strip.setPixelColor(23, Color(255, 255, 255))

        strip.setPixelColor(2, Color(255, 255, 255))
        strip.setPixelColor(5, Color(255, 255, 255))
        strip.setPixelColor(8, Color(255, 255, 255))
        strip.setPixelColor(11, Color(255, 255, 255))

        strip.setPixelColor(14, Color(255, 255, 255))
        strip.setPixelColor(17, Color(255, 255, 255))
        strip.setPixelColor(20, Color(255, 255, 255))
        strip.setPixelColor(23, Color(255, 255, 255))

	strip.setPixelColor(25, Color(0, 255, 255))
	strip.setPixelColor(26, Color(0, 255, 255))
	strip.setPixelColor(27, Color(0, 255, 255))
	strip.setPixelColor(28, Color(0, 255, 255))
	strip.setPixelColor(29, Color(0, 255, 255))
	strip.setPixelColor(30, Color(0, 255, 255))
	strip.setPixelColor(33, Color(0, 255, 255))
	strip.setPixelColor(38, Color(0, 255, 255))
	strip.show()

def blink(strip, blinkSpeed=100):
	min = 1
	max = 24
	count = 0

	while (count < max):
		strip.setPixelColor(count, Color( 255, 255, 255))
		count=count+1

	strip.setPixelColor(3, Color(0, 0, 0))
	strip.setPixelColor(4, Color(0, 0, 0))
	strip.setPixelColor(9, Color(0, 0, 0))
	strip.setPixelColor(10, Color(0, 0, 0))

	strip.setPixelColor(15, Color(0, 0, 0))
	strip.setPixelColor(16, Color(0, 0, 0))
	strip.setPixelColor(22, Color(0, 0, 0))
	strip.setPixelColor(21, Color(0, 0, 0))

	strip.setPixelColor(31, Color(0, 0, 0))
	strip.setPixelColor(30, Color(0, 0, 0))
	strip.setPixelColor(25, Color(0, 0, 0))
	strip.setPixelColor(24, Color(0, 0, 0))

	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))

	strip.show()

	time.sleep(1)

	strip.setPixelColor(3, Color(0, 0, 0))
	strip.setPixelColor(4, Color(0, 0, 0))
	strip.setPixelColor(9, Color(0, 0, 0))
	strip.setPixelColor(10, Color(0, 0, 0))

	strip.setPixelColor(15, Color(0, 0, 0))
	strip.setPixelColor(16, Color(0, 0, 0))
	strip.setPixelColor(22, Color(0, 0, 0))
	strip.setPixelColor(21, Color(0, 0, 0))

	strip.show()
	time.sleep(blinkSpeed/1000.0)

	strip.setPixelColor(2, Color(0, 0, 0))
	strip.setPixelColor(5, Color(0, 0, 0))
	strip.setPixelColor(8, Color(0, 0, 0))
	strip.setPixelColor(11, Color(0, 0, 0))

	strip.setPixelColor(14, Color(0, 0, 0))
	strip.setPixelColor(17, Color(0, 0, 0))
	strip.setPixelColor(20, Color(0, 0, 0))
	strip.setPixelColor(23, Color(0, 0, 0))

	strip.show()
	time.sleep(blinkSpeed/1000.0)

	## there is a bug here
	## pixel 1 won't do anything unless pixel 0 gets the same action
	strip.setPixelColor(0, Color(0, 0, 0))
	strip.setPixelColor(1, Color(0, 0, 0))
	strip.setPixelColor(6, Color(0, 0, 0))
	strip.setPixelColor(7, Color(0, 0, 0))
	strip.setPixelColor(12, Color(0, 0, 0))

	strip.setPixelColor(13, Color(0, 0, 0))
	strip.setPixelColor(18, Color(0, 0, 0))
	strip.setPixelColor(19, Color(0, 0, 0))
	strip.setPixelColor(23, Color(0, 0, 0))

	strip.show()
	time.sleep(blinkSpeed/1000.0)

	strip.setPixelColor(0, Color(255, 255, 255))
	strip.setPixelColor(1, Color(255, 255, 255))
	strip.setPixelColor(6, Color(255, 255, 255))
	strip.setPixelColor(7, Color(255, 255, 255))
	strip.setPixelColor(12, Color(255, 255, 255))

	strip.setPixelColor(13, Color(255, 255, 255))
	strip.setPixelColor(18, Color(255, 255, 255))
	strip.setPixelColor(19, Color(255, 255, 255))
	strip.setPixelColor(23, Color(255, 255, 255))

	strip.show()
	time.sleep(blinkSpeed/1000.0)

	strip.setPixelColor(2, Color(255, 255, 255))
	strip.setPixelColor(5, Color(255, 255, 255))
	strip.setPixelColor(8, Color(255, 255, 255))
	strip.setPixelColor(11, Color(255, 255, 255))

	strip.setPixelColor(14, Color(255, 255, 255))
	strip.setPixelColor(17, Color(255, 255, 255))
	strip.setPixelColor(20, Color(255, 255, 255))
	strip.setPixelColor(23, Color(255, 255, 255))
	strip.show()
	time.sleep(blinkSpeed/1000.0)

def wakeup(strip):
	min = 1
	max = 24
	count = 0

	while (count < max):
		strip.setPixelColor(count, Color( 16, 16, 16))
		count=count+1
	strip.show()

	strip.setPixelColor(3, Color( 255, 255, 255))
	strip.setPixelColor(4, Color( 255, 255, 255))
	strip.setPixelColor(15, Color( 255, 255, 255))
	strip.setPixelColor(16, Color( 255, 255, 255))

	strip.setPixelColor(26, Color( 0, 32, 32))
	strip.setPixelColor(27, Color( 0, 32, 32))
	strip.setPixelColor(28, Color( 0, 32, 32))
	strip.setPixelColor(29, Color( 0, 32, 32))
	strip.setPixelColor(35, Color( 0, 32, 32))
	strip.setPixelColor(36, Color( 0, 32, 32))
	
	strip.show()
	time.sleep(200/1000.0)

	strip.setPixelColor(2, Color( 255, 255, 255))
	strip.setPixelColor(5, Color( 255, 255, 255))
	strip.setPixelColor(14, Color( 255, 255, 255))
	strip.setPixelColor(17, Color( 255, 255, 255))
	
	strip.setPixelColor(35, Color( 0, 128, 128))
	strip.setPixelColor(36, Color( 0, 128, 128))
	strip.show()
	time.sleep(200/1000.0)

	strip.setPixelColor(1, Color( 255, 255, 255))
	strip.setPixelColor(6, Color( 255, 255, 255))
	strip.setPixelColor(13, Color( 255, 255, 255))
	strip.setPixelColor(18, Color( 255, 255, 255))
	
	strip.setPixelColor(35, Color( 0, 32, 32))
	strip.setPixelColor(36, Color( 0, 32, 32))
	strip.show()
	time.sleep(200/1000.0)
	
	strip.setPixelColor(12, Color( 255, 255, 255))
	strip.setPixelColor(7, Color( 255, 255, 255))
	strip.setPixelColor(24, Color( 255, 255, 255))
	strip.setPixelColor(19, Color( 255, 255, 255))
	
	strip.setPixelColor(35, Color( 0, 192, 192))
	strip.setPixelColor(36, Color( 0, 192, 192))
	strip.show()
	time.sleep(200/1000.0)
	
	strip.setPixelColor(11, Color( 255, 255, 255))
	strip.setPixelColor(8, Color( 255, 255, 255))
	strip.setPixelColor(23, Color( 255, 255, 255))
	strip.setPixelColor(20, Color( 255, 255, 255))
	
	strip.setPixelColor(35, Color( 0, 96, 96))
	strip.setPixelColor(36, Color( 0, 96, 96))
	strip.show()
	time.sleep(200/1000.0)
	
	strip.setPixelColor(10, Color( 255, 255, 255))
	strip.setPixelColor(9, Color( 255, 255, 255))
	strip.setPixelColor(22, Color( 255, 255, 255))
	strip.setPixelColor(21, Color( 255, 255, 255))

	max = 40
	count = 24
	while (count < max):
		strip.setPixelColor(count, Color( 0, 192, 192))
		count=count+1
	strip.show()
	time.sleep(200/1000.0)

	max = 40
	count = 24
	while (count < max):
		strip.setPixelColor(count, Color( 0, 0, 0))
		count=count+1
	strip.show()
	time.sleep(200/1000.0)
	
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.show()
	time.sleep(200/1000.0)
	
	

def knightRider(strip, red, green, blue):
	count = 0

	while (count < 6):
		ct = 0
		while (ct < 8):
			strip.setPixelColor(ct-3, Color( 0, 0, 0))
			strip.setPixelColor(ct-2, Color( red / 8, green / 8, blue / 8))
			strip.setPixelColor(ct-1, Color( red / 4, green / 4, blue / 4))
			strip.setPixelColor(ct, Color( red, green, blue))
			strip.setPixelColor(ct+1, Color( red / 4, green / 4, blue / 4))
			strip.show()
			time.sleep(50/1000.0)
			ct = ct + 1
		while (ct > -1):
			strip.setPixelColor(ct+3, Color( 0, 0, 0))
			strip.setPixelColor(ct+2, Color( red / 8, green / 8, blue / 8))
			strip.setPixelColor(ct+1, Color( red / 4, green / 4, blue / 4))
			strip.setPixelColor(ct, Color( red, green, blue))
			strip.setPixelColor(ct-1, Color( red / 4, green / 4, blue / 4))
			strip.show()
			time.sleep(50/1000.0)
			ct = ct - 1
		count = count + 1

def align(strip):
	strip.setPixelColor(0, Color( 255, 255, 255))
	strip.setPixelColor(5, Color( 255, 255, 255))
	strip.setPixelColor(12, Color( 255, 255, 255))
	strip.setPixelColor(23, Color( 255, 255, 255))
	strip.setPixelColor(24, Color( 255, 255, 255))
	strip.setPixelColor(31, Color( 255, 255, 255))
	strip.setPixelColor(32, Color( 255, 255, 255))
	strip.setPixelColor(39, Color( 255, 255, 255))
	strip.show();

def selfTest(strip):
	count = 0

	while (count < 40):
		strip.setPixelColor(count - 1, Color( 0, 0, 0))
		strip.setPixelColor(count, Color( 255, 255, 255))
		strip.show()
		count = count + 1
		time.sleep(50/1000.0)

	strip.setPixelColor(39, Color( 0, 0, 0))
	strip.show()

def clear(strip):
	count = 0

	while (count < 40):
		strip.setPixelColor(count, Color( 0, 0, 0))
		strip.show()
		count = count + 1

def talk(strip):
        strip.setPixelColor(0, Color(255, 255, 255))
        strip.setPixelColor(1, Color(255, 255, 255))
        strip.setPixelColor(6, Color(255, 255, 255))
        strip.setPixelColor(7, Color(255, 255, 255))
        strip.setPixelColor(12, Color(255, 255, 255))

        strip.setPixelColor(13, Color(255, 255, 255))
        strip.setPixelColor(18, Color(255, 255, 255))
        strip.setPixelColor(19, Color(255, 255, 255))
        strip.setPixelColor(23, Color(255, 255, 255))

        strip.setPixelColor(2, Color(255, 255, 255))
        strip.setPixelColor(5, Color(255, 255, 255))
        strip.setPixelColor(8, Color(255, 255, 255))
        strip.setPixelColor(11, Color(255, 255, 255))

        strip.setPixelColor(14, Color(255, 255, 255))
        strip.setPixelColor(17, Color(255, 255, 255))
        strip.setPixelColor(20, Color(255, 255, 255))
        strip.setPixelColor(23, Color(255, 255, 255))
        strip.show()

	strip.setPixelColor(38, Color(0, 255, 255))
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.setPixelColor(33, Color(0, 255, 255))
	strip.show()
	time.sleep(200/1000.0)

	strip.setPixelColor(38, Color(0, 0, 0))
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.setPixelColor(33, Color(0, 0, 0))
	strip.show()
	time.sleep(200/1000.0)
	
	strip.setPixelColor(38, Color(0, 255, 255))
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.setPixelColor(33, Color(0, 255, 255))
	strip.show()
	time.sleep(200/1000.0)

	strip.setPixelColor(38, Color(0, 0, 0))
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.setPixelColor(33, Color(0, 0, 0))
	strip.show()
	time.sleep(200/1000.0)
	
	strip.setPixelColor(38, Color(0, 255, 255))
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.setPixelColor(33, Color(0, 255, 255))
	strip.show()
	
	strip.setPixelColor(38, Color(0, 0, 0))
	strip.setPixelColor(37, Color(0, 255, 255))
	strip.setPixelColor(36, Color(0, 255, 255))
	strip.setPixelColor(35, Color(0, 255, 255))
	strip.setPixelColor(34, Color(0, 255, 255))
	strip.setPixelColor(33, Color(0, 0, 0))
	strip.show()
	
# Main program logic follows:
if __name__ == '__main__':
	# Create NeoPixel object with appropriate configuration.
	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
	# Intialize the library (must be called once before other functions).
	strip.begin()

	if sys.argv[1] == 'selfTest':
		selfTest(strip)
	if sys.argv[1] == 'blink':
		blink(strip, 100)
	if sys.argv[1] == 'alignment':
		align(strip)
	if sys.argv[1] == 'knightRider':
		knightRider(strip, 255, 0, 0)
	if sys.argv[1] == 'wakeup':
		wakeup(strip)
		blink(strip, 200)
	if sys.argv[1] == 'neutralFace':
		neutralFace(strip)
	if sys.argv[1] == 'smile':
		smile(strip)
	if sys.argv[1] == 'frown':
		frown(strip)
	if sys.argv[1] == 'clear':
		clear(strip)
	if sys.argv[1] == 'talk':
		talk(strip)
