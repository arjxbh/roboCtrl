#!/usr/bin/env python
# coding: Latin-1

import time
import sys
import sys, getopt

from neopixel import *

# LED strip configuration:
LED_COUNT      = 32      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (must support PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 5       # DMA channel to use for generating signal (try 5)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)

# Define functions which animate LEDs in various ways.
def knightRider(strip, red, green, blue):
	count = 0

	while (count < 20):
		ct = 24 
		while (ct < 32):
			strip.setPixelColor(ct-3, Color( 0, 0, 0))
			strip.setPixelColor(ct-2, Color( red / 8, green / 8, blue / 8))
			strip.setPixelColor(ct-1, Color( red / 4, green / 4, blue / 4))
			strip.setPixelColor(ct, Color( red, green, blue))
			strip.setPixelColor(ct+1, Color( red / 4, green / 4, blue / 4))
			strip.show()
			time.sleep(50/1000.0)
			ct = ct + 1
		while (ct > 26):
			strip.setPixelColor(ct+3, Color( 0, 0, 0))
			strip.setPixelColor(ct+2, Color( red / 8, green / 8, blue / 8))
			strip.setPixelColor(ct+1, Color( red / 4, green / 4, blue / 4))
			strip.setPixelColor(ct, Color( red, green, blue))
			strip.setPixelColor(ct-1, Color( red / 4, green / 4, blue / 4))
			strip.show()
			time.sleep(50/1000.0)
			ct = ct - 1
		count = count + 1
		

def selfTest(strip):
	count = 0

	while (count < 8):
		strip.setPixelColor(count - 1, Color( 0, 0, 0))
		strip.setPixelColor(count, Color( 255, 255, 255))
		strip.show()
		count = count + 1
		time.sleep(300/1000.0)

	strip.setPixelColor(8, Color( 0, 0, 0))
	
# Main program logic follows:
if __name__ == '__main__':
	# Create NeoPixel object with appropriate configuration.
	strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS)
	# Intialize the library (must be called once before other functions).
	strip.begin()

	print 'Press Ctrl-C to quit.'
	#selfTest(strip)
	knightRider(strip, 255, 0, 0)
