#!/usr/bin/php
<?php
require_once('./head.php');

//get queue type from command line argument
$queueType = $argv[1];

echo 'starting queue consumer for '.$queueType;

if($queueType == 'move'){
	require_once($SERVO_CFG);
	$servoPositions = new position();

	foreach($servoList as $name => $config){
		$servoPositions->createServo($config['index'], $name, $config['min'], $config['max'], $config['neutral']);
	}
}

$roboQueue = new queue($QUEUE_PATH, $MODEL_PATH);
$roboQueue->eraseQueue($queueType);

while($roboQueue->getRunState()){
	$roboQueue->readQueue($queueType);
	sleep(0.3);
}
