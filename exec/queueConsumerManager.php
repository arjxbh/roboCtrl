#!/usr/bin/php
<?php
require_once('./head.php');

// this should run out of the cron every minute and check
// if all the queue Consumers are running.  If they are not
// running, then it should launch them

// define all the queue types
$queueTypes = array('talk', 'walk', 'move', 'express', 'system')

// check if any queues are running.
// if the count is 0, then clear any stops

$numConsumers = exec( 'ps -ef | grep queueConsumer.php | grep -v grep | wc -l' );
echo 'There are '.$numConsumers.' consumers currently running';

if($numConsumers == 0){
  $roboQueue = new queue($QUEUE_PATH, $MODEL_PATH);
  $roboQueue->eraseQueue('stop');
}

// check if queues are running.  If a queue isn't running, then
// clear all queued items for that queue and then start it
foreach($queueTypes as $type){
  $queueCheck = exec( 'ps -ef | grep queueConsumer.php | grep -v grep | grep '.$type.' | wc -l' );
  if( $queueCheck == 0 ){
    $roboQueue = new queue($QUEUE_PATH, $MODEL_PATH);
    $roboQueue->eraseQueue($type);
    exec( $BASE_DIR.'/exec/queueConsumer.php '.$type);
    // should this be moved to the class???? ^^^
  }
}
