#!/bin/ksh

exec 2>/dev/null

#exec 2> $0.log.$$
#PS4='$LINENO $SECONDS: '
#set -x

queueType=$1
queueDir=$2
execDir="$3/exec"
logFile=$4

queueCount=0

ls $queueDir/*.q | while read filename; do
  (( queueCount++ ))
  baseFileName=${filename/'.q'/}
  baseFileName=${baseFileName//*'/'/}
  queueFileType=${baseFileName#*.}
  queueFileType=${queueFileType%%.*}

  [[ $queueFileType == 'stop' ]] && print 'stop'
  [[ $queueFileType == 'stop' ]] && exit 0
  [[ $queueFileType != $queueType && $queueFileType != 'READONLY' ]] && continue

  #microtime=${baseFileName%%.*}
  #action=${baseFileName##*.}

  [[ $queueFileType != 'READONLY' ]] && mv ${filename} $queueDir/${baseFileName}.p
  #print ${baseFileName}.p
  print ${baseFileName}
  exit 0
done

if[[$queueCount -eq 0]]; then
  print 'NOQUEUE'
fi
