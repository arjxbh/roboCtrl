#!/bin/ksh

#exec 2> $0.log.$$
#PS4='$LINENO $SECONDS: '
#set -x

. /export/home/swift/vizgems/data/www/proj/vg/cgi-bin-members/tiled/tilehome.cfg

typeset -F4 SCRW GWSIZE SCALE GHSIZE HSCALE WSCALE

datadir=$1
FDATE=$4
LDATE=$5
SCRW=$2
SCRH=$3
(( SCRH = SCRH - 100 ))
VERBOSE=0
## VERBOSITY = 0 - 2

#print "W: $SCRW, H: $SCRH<br>"

## calculate scale based on width
GWSIZE=`grep "bb=" $datadir/debug.log | head -1 | awk -F',' '{print $3}'`
[[ $SCRW -gt $GWSIZE ]] && (( SCRW = SCRW * .95 ))
(( WSCALE = SCRW / GWSIZE ))

## calculate scale based on height
GHSIZE=`grep "bb=" $datadir/debug.log | head -1 | awk -F',' '{print $4}'`
[[ $SCRH -gt $GHSIZE ]] && (( SCRH = SCRH * .95 ))
(( HSCALE = SCRH / GHSIZE ))

## select smaller scale of two
if [[ $HSCALE -lt $WSCALE ]] then
	SCALE=$HSCALE
else
	SCALE=$WSCALE
fi

## make sure that the width takes up at least 30% of the screen space
typeset -F2 WPERC
(( WPERC = ( GWSIZE * SCALE ) / SCRW ))
[[ $WPERC -lt 0.3 ]] && (( SCALE = ( HSCALE + WSCALE ) * 0.2 ))

## set min and max bounds for scaling
[[ $SCALE -gt 2.5 ]] && SCALE=2.5
[[ $SCALE -lt 0.5 ]] && SCALE=0.5
#print "<br>SCALE: $SCALE<br>"

## scale page elements to match
(( ICONSIZE = 25 * SCALE ))
(( NODEFONTSIZE = 10 * SCALE ))
(( SMALLNODEFONTSIZE = 8 * SCALE ))
(( CAPTIONSIZE = 12 * SCALE ))
(( BTNSIZE = 14 * SCALE ))
(( BTNFONTSIZE = 10 * SCALE ))
(( ALARMBTNOFFSET = 16 * SCALE ))
(( HZOOM = 1.5 / SCALE ))
[[ HZOOM -lt 1.2 ]] && HZOOM=1.2

#print "NODEFONTSIZE: $NODEFONTSIZE<br>"

[[ $VERBOSE -ge 1 ]] && print "<br>$datadir<br>"
#print "<br>$datadir<br>"
#print "<br>Screen width: $SCRW<br>"
#print "<br>Screen height: $SCRH<br>"

if [[ $datadir == "" ]] then
	[[ $VERBOSE -ge 1 ]] && print "NO INPUT DIRECTORY!"
	exit 0
fi

typeset -A graphdata unq linedata unqlines

## This is used to preserve the order of the input file on output
## This way children subgraphs and nodes are written to the correct parent digraphs
ORDERCT=0
unset lineorder

## This is used to track what container splines belong to
PARENT=""

## read graphviz output and store data to arrays for processesing

cat $datadir/debug.log | sed 's/\\n/<br>/g' | while read key line; do
	ky=${key/'='*/}
	case $ky in
		"digraph")
			k1=${line/' {'/}
			k1="graph_$k1"
			[[ $VERBOSE -ge 2 ]] && print "set key1 = $k1<br>"
			unq[$k1]=1
			lineorder[$ORDERCT]=$k1
			(( ORDERCT++ ))
			PARENT=$k1
		;;
		"label")
			k2=$ky
			val=${key/*'='/}$line
			val=${val//','/}
			val=${val//'"'/}

			graphdata[$k1:$k2]=$val
			[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:$k2]=$val<br>"
		;;
		"objname")
			k2=$ky
			val=${key/*'='/}$line
			val=${val//','/}
			val=${val//'"'/}
			lvl=${val#*:}
			lvl=${lvl%%:*}
			val=${val#*':'}
			val=${val#*':'}

			graphdata[$k1:$k2]=$val
			graphdata[$k1:level]=$lvl
			[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:$k2]=$val<br>"
			[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:$level]=$lvl<br>"

		;;
		"subgraph")
			k1=${line/' {'/}
			k1="subgraph_$k1"
			[[ $VERBOSE -ge 2 ]] && print "set key1 = $k1<br>"
			unq[$k1]=1
			lineorder[$ORDERCT]=$k1
			(( ORDERCT++ ))
		;;
		"n"*)
			[[ $ky == "nodesep" ]] && continue
			[[ $ky == "node" ]] && continue
			ckarrow=${line/' '*/}
			if [[ $ckarrow != "->" ]] then
				k1=$ky
				[[ $VERBOSE -ge 2 ]] && print "set key1 = $k1<br>"
				unq[$k1]=1
				lineorder[$ORDERCT]=$k1
				(( ORDERCT++ ))

				info=${line/*'info='/}
				info=${info/','*/}
				info=${info//'"'/}
				graphdata[$k1:info]=$info
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:info]=$info<br>"

				objn=${line/*'objname='/}
				objn=${objn/','*/}
				objn=${objn//'"'/}
				objn=${objn#*:}
				objn=${objn#*:}
				graphdata[$k1:objname]=$objn
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:objname]=$objn<br>"

				lvl=${line/*'objname='/}
				lvl=${lvl#*:}
				lvl=${lvl%%:*}
				graphdata[$k1:level]=$lvl
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:level]=$lvl<br>"

				typen=${line/*'typename='/}
				typen=${typen/','*/}
				typen=${typen//'"'/}
				graphdata[$k1:typename]=$typen
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:typename]=$typen<br>"

				ht=${line/*'height='/}
				ht=${ht/','*/}
				ht=${ht//'"'/}
				(( ht = ht * 72 ))
				(( ht = ht * SCALE ))
				graphdata[$k1:height]=$ht
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:height]=$ht<br>"

				wd=${line/*'width='/}
				wd=${wd/','*/}
				wd=${wd//'"'/}
				(( wd = wd * 72 ))
				(( wd = wd * SCALE ))
				graphdata[$k1:width]=$wd
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:width]=$wd<br>"

				dn=${line/*'dispname='/}
				dn=${dn/','*/}
				dn=${dn//'"'/}
				graphdata[$k1:display]=$dn
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:display]=$dn<br>"


				pos=${line/*'pos='/}
				pos=${pos%,*}
				pos=${pos//'"'/}
				x=${pos/*,/}
				y=${pos/,*/}
				(( x = x * SCALE ))
				(( y = y * SCALE ))
				(( x = x + ( ht / 2 ) ))
				(( y = y - ( wd / 2 ) ))

				(( x = totalh - x ))

				graphdata[$k1:x]=$y
				graphdata[$k1:y]=$x

				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:x]=$x<br>"
				[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:y]=$y<br>"
			else
				## get end point id
				ep=${line#*' '}
				ep=${ep%%' '*}
				## get start point id
				sp=$ky

				posstr=${line/*'pos='/}
				posstr=${posstr#*'"'}
				posstr=${posstr/'"'*/}

				scoords=${posstr/' e,'*/}
				scoords=${scoords/*'s,'/}
				sx=${scoords/,*/}
				sy=${scoords/*,/}
				(( sx = sx * SCALE ))
				(( sy = sy * SCALE ))
				(( sy = totalh - sy ))

				ecoords=${posstr/*'e,'/}
				set -A cArry $ecoords
				cstring=""

				for idx in ${!cArry[@]}; do
					pair=${cArry[$idx]}
					ex=${pair/,*/}
					ey=${pair/*,/}
					(( ex = ex * SCALE ))
					(( ey = ey * SCALE ))
					(( ey = totalh - ey ))
					cstring=$cstring$ex","$ey" "
				done

				cstring=${cstring%' '}

				unqlines[$sp:$ep]=1
				linedata[$sp:$ep:sx]=$sx
				linedata[$sp:$ep:sy]=$sy
				#linedata[$sp:$ep:ex]=$ex
				#linedata[$sp:$ep:ey]=$ey

				[[ $VERBOSE -ge 2 ]] && print "set linedata[$sp:$ep:sx]=$sx<br>"
				[[ $VERBOSE -ge 2 ]] && print "set linedata[$sp:$ep:sy]=$sy<br>"
				#[[ $VERBOSE -ge 2 ]] && print "set linedata[$sp:$ep:ex]=$ex<br>"
				#[[ $VERBOSE -ge 2 ]] && print "set linedata[$sp:$ep:ey]=$ey<br>"

				linedata[$sp:$ep:exy]=$cstring
				[[ $VERBOSE -ge 2 ]] && print "set linedata[$sp:$ep:exy]=$cstring<br>"

				linedata[$sp:$ep:parent]=$PARENT
				[[ $VERBOSE -ge 2 ]] && print "set linedata[$sp:$ep:parent]=$parent<br>"

			fi
		;;
		bb)
			values=${key/*'='/}
			values=${values//'"'/}
			values=${values%,*}

			x=${values%%,*}
			y=${values#*,}
			width=${y#*,}
			y=${y%%,*}
			width=${width%%,*}
			height=${values##*,}
			(( width = width - x ))
			(( height = height - y ))
			(( x = x * SCALE ))
			(( y = y * SCALE ))
			(( width = width * SCALE ))
			(( height = height * SCALE ))

			if [[ $k1 == "graph"* ]] then
				totalh=$height
				totalw=$width
			else
				(( y = totalh - y - height ))
			fi

			graphdata[$k1:x]=$x
			graphdata[$k1:y]=$y
			graphdata[$k1:width]=$width
			graphdata[$k1:height]=$height

			[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:x]=$x<br>"
			[[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:y]=$y<br>"
                        [[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:width]=$width<br>"
                        [[ $VERBOSE -ge 2 ]] && print "set graphdata[$k1:height]=$height<br>"
		;;
	esac
done

cat <<EOF
<style type="text/css">
.vgGroup{
	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=75)";
	filter: alpha(opacity=75);
	-moz-opacity: 0.75;
	-khtml-opacity: 0.75;
	opacity: 0.75;
	background-color: #f3f1f1;

	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;

	-moz-box-shadow: 0px 0px 4px 2px #888888;
	-webkit-box-shadow: 0px 0px 4px 2px #888888;
	box-shadow: 0px 0px 4px 2px #888888;

	z-index: 2;
	position: absolute;
	text-align: center;
}
.vgGroupCaption{
	background-color: #fff;

	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	border-radius: 5px;

	z-index: 2;
	position: relative;
	text-align: center;

	border-radius: 2px 2px 2px 2px;
	-moz-border-radius: 2px 2px 2px 2px;
	-webkit-border-radius: 2px 2px 2px 2px;
	border: 0px solid #bababa;

	font-size: ${CAPTIONSIZE}px;
	line-height: 90%;
}
.vgGroupCaption:hover{
	cursor: pointer;
	border: 1px solid;
}
.vgGraphTitle{
	z-index: 1;
	font-size: 14px;
	line-height: 90%;
}
.vgGraphTitle:hover{
	cursor: pointer;
}
.vgNodeElement{
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;

        z-index: 4;
        position: relative;
	float: left;
        text-align: center;

        border-radius: 2px 2px 2px 2px;
        -moz-border-radius: 2px 2px 2px 2px;
        -webkit-border-radius: 2px 2px 2px 2px;
        border: 0px solid #bababa;
}
.vgNodeText{
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;

        z-index: 4;
        position: relative;
	/*float: left;*/
        text-align: center;

        border-radius: 2px 2px 2px 2px;
        -moz-border-radius: 2px 2px 2px 2px;
        -webkit-border-radius: 2px 2px 2px 2px;
        border: 0px solid #bababa;

	font-size: ${NODEFONTSIZE}px;
	line-height: 80%;
	border: 1px solid;

	padding: 1px 1px 1px 1px;
}
.statbtn{
	width: ${BTNSIZE}px;
	height: ${BTNSIZE}px;
	z-index: 5;
	border: 1px solid;

        -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=75)";
        filter: alpha(opacity=75);
        -moz-opacity: 0.75;
        -khtml-opacity: 0.75;
        opacity: 0.75;
	float: left;

	border-radius: 5px 5px 5px 5px;
        -moz-border-radius: 5px 5px 5px 5px;
        -webkit-border-radius: 5px 5px 5px 5px;

	background-color: #fcb314;

	text-align: center;
	vertical-align: middle;

	font-size: ${BTNFONTSIZE}px;
}
.statbtn:hover{
	background-color: #ff7200;
	cursor: pointer;
}
.vgNode{
	padding: 0 0 0 0;
	position: absolute;
	z-index: 3;
	background-color: transparent;
	text-align: center;
}
.vgNode:hover{
	-ms-transform: scale($HZOOM,$HZOOM); /* IE 9 */
	-webkit-transform: scale($HZOOM,$HZOOM); /* Chrome, Safari, Opera */
	transform: scale($HZOOM,$HZOOM);

	z-index: 6;

	-moz-box-shadow: 0px 0px 4px 2px #888888;
	-webkit-box-shadow: 0px 0px 4px 2px #888888;
	box-shadow: 0px 0px 4px 2px #888888;

	background-color: #fff;

	-ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=90)";
	filter: alpha(opacity=90);
	-moz-opacity: 0.9;
	-khtml-opacity: 0.9;
	opacity: 0.9;
}
.vgNodeHighlight{
	-ms-transform: scale(1.1,1.1); /* IE 9 */
	-webkit-transform: scale(1.1,1.1); /* Chrome, Safari, Opera */
	transform: scale(1.1,1.1);

	z-index: 5;

	-moz-box-shadow: 0px 0px 4px 2px #888888;
	-webkit-box-shadow: 0px 0px 4px 2px #888888;
	box-shadow: 0px 0px 4px 2px #888888;
}
</style>
EOF


## this is to keep track of the current master container
CURGRAPH=""
for ordkey in ${!lineorder[@]}; do
	key=${lineorder[$ordkey]}
	case $key in
		"graph"*)
			## this if statment completes the previous graph
			if [[ $CURGRAPH != "" ]] then
				print "</div>"
				gheight=${graphdata[$CURGRAPH:height]}
				(( canvasOffset = gheight * -1 ))
				canvasOffset=$canvasOffset"px"
				print "<div id=\"graphvizSplines_$CURGRAPH\" style=\"margin-left: auto; margin-right: auto; width: ${graphdata[$CURGRAPH:width]}px; height: ${graphdata[$CURGRAPH:height]}px; margin-top: $canvasOffset;\">"
				print "<canvas id=\"canvas_$CURGRAPH\" width=\"${graphdata[$CURGRAPH:width]}px\" height=\"${graphdata[$CURGRAPH:height]}px\">Error: Canvas not supported</canvas>"
print "</div>"

			fi

			## this part starts a new graph
			print "<div style=\"position: relative; left: ${graphdata[$key:x]}px; top: ${graphdata[$key:y]}; width: ${graphdata[$key:width]}px; height: ${graphdata[$key:height]}px; z-index: 1; background-color: transparent; text-align: center; margin-left: auto; margin-right: auto; border: 1px solid #DFDFDF;\">"

			linkhtml=`grep "${graphdata[$key:objname]}" $datadir/invgraph.*.cmap`
			targetqid=${linkhtml/*'qid='/}
			targetqid=${targetqid/'&'*/}
			targetqid=${targetqid/'"'*/}

			## this is an exception for CRE queries to link back to ALL customers rather than one.  Need to find a way to make this more generic for other services
			if [[ $targetqid == "cre_"* && ${graphdata[$key:level]} == "c" ]] then

				print "<span class=\"vgGraphTitle\" onclick=\"jqAjax('/cgi-bin-vg-members/tiled/vg_runQuery.cgi?lod='+QLOD+'&qid=$targetqid&obj=[CRE]&fdate=$FDATE&ldate=$LDATE&lvl=level_b','querytool','true'); setQvars(QLOD,'$FDATE','$LDATE','$targetqid','level_b','[CRE]');\" style=\"background-color: transparent; text-decoration: none;\">${graphdata[$key:label]}</span>"

			else
				print "<span class=\"vgGraphTitle\" onclick=\"jqAjax('/cgi-bin-vg-members/tiled/vg_runQuery.cgi?lod='+QLOD+'&qid=$targetqid&obj=${graphdata[$key:objname]}&fdate=$FDATE&ldate=$LDATE&lvl=level_${graphdata[$key:level]}','querytool','true'); setQvars(QLOD,'$FDATE','$LDATE','$targetqid','level_${graphdata[$key:level]}','${graphdata[$key:objname]}');\" style=\"background-color: transparent; text-decoration: none;\">${graphdata[$key:label]}</span>"

			fi

			CURGRAPH=$key
		;;
		"subgraph"*)
			if [[ ${graphdata[$key:x]} != "" && ${graphdata[$key:width]} -gt 0 ]] then
				print "<div style=\"left: ${graphdata[$key:x]}px; top: ${graphdata[$key:y]}px; width: ${graphdata[$key:width]}px; height: ${graphdata[$key:height]}px;\" class=\"vgGroup\">"
				linkhtml=`grep "${graphdata[$key:objname]}" $datadir/invgraph.*.cmap`
				targetqid=${linkhtml/*'qid='/}
				targetqid=${targetqid/'&'*/}
				targetqid=${targetqid/'"'*/}

				print "	<div class=\"vgGroupCaption\">"
				print "<span onclick=\"jqAjax('/cgi-bin-vg-members/tiled/vg_runQuery.cgi?lod='+QLOD+'&qid=$targetqid&obj=${graphdata[$key:objname]}&fdate=$FDATE&ldate=$LDATE&lvl=level_${graphdata[$key:level]}','querytool','true'); setQvars(QLOD,'$FDATE','$LDATE','$targetqid','level_${graphdata[$key:level]}','${graphdata[$key:objname]}');\" style=\"background-color: transparent; text-decoration: none;\">"
				print "		${graphdata[$key:label]}"
				print "</span>"
				if [[ $VGgroupStats == "true" ]] then
					obj=${graphdata[$key:objname]}
		                        bldg=${obj/'-'*/}
                		        ast=${obj/*'-'/}
		                        typ=${ast:0:3}
                		        rest=${ast:3:100}
		                        rest=${rest/n/g}

                		        groupobj="$bldg-$typ$rest"

					print "<div style=\"position: absolute; right: 0px; top: 0px;\"class=\"statbtn\" onclick='togglestatmenu(\"$groupobj\",\"$FDATE\",\"$LDATE\");'><i class=\"fa fa-line-chart\"></i></div>"
				fi
				print "	</div>"
				print "</div>"
			fi
		;;
		*)
			print "<div class=\"vgNode\" id=\"${graphdata[$key:objname]}\" style=\"left: ${graphdata[$key:x]}px; top: ${graphdata[$key:y]}px; width: ${graphdata[$key:width]}px; height: ${graphdata[$key:height]}px;\" onmouseover=\"showEdges('${graphdata[$key:objname]}');\" onmouseout=\"hideEdges('${graphdata[$key:objname]}');\">"

			linkhtml=`grep "${graphdata[$key:objname]}" $datadir/invgraph.*.cmap`
			targetqid=${linkhtml/*'qid='/}
			targetqid=${targetqid/'&'*/}
			targetqid=${targetqid/'"'*/}

			print "<a href=\"#\" onclick=\"jqAjax('/cgi-bin-vg-members/tiled/vg_runQuery.cgi?lod='+QLOD+'&qid=$targetqid&obj=${graphdata[$key:objname]}&fdate=$FDATE&ldate=$LDATE&lvl=level_${graphdata[$key:level]}','querytool','true'); setQvars(QLOD,'$FDATE','$LDATE','$targetqid','level_${graphdata[$key:level]}','${graphdata[$key:objname]}');\" style=\"background-color: transparent; text-decoration: none;\">"

			if [[ -f /export/home/swift/vizgems/data/www/htdocs/proj/vg/tiled/images/${graphdata[$key:typename]}.png ]] then
				print "<img src=/proj/vg/tiled/images/${graphdata[$key:typename]}.png border=0 width=${ICONSIZE}px height=${ICONSIZE}px style=\"background-color: transparent;\">"
			else
				if [[ ${graphdata[$key:level]} == "o" || ${graphdata[$key:level]} == "g" ]] then
					print "<img src=/proj/vg/tiled/images/NO.ICON.png border=0 width=${ICONSIZE}px height=${ICONSIZE}px style=\"background-color: transparent;\">"
				fi
				if [[ $targetqid == "cre_linear" ]] then
					print "<img src=/proj/vg/tiled/images/CITY.png border=0 width=${ICONSIZE}px height=${ICONSIZE}px style=\"background-color: transparent;\">"
				fi
			fi

			alarmcolor=`grep "|${graphdata[$key:objname]}|" $datadir/alarmstyle.embed | awk -F'|' '{print $7}'`
			[[ $alarmcolor == "" ]] && alarmcolor="#fff"

			print "<div class=\"vgNodeText\" style=\"background-color: $alarmcolor;\">"
			print "${graphdata[$key:display]}"
			[[ ${graphdata[$key:level]} == "o" && $VGShowRPN == "true" ]] && print "<span style=\"font-size: ${SMALLNODEFONTSIZE}px;\">rRPN:&nbsp;&nbsp;&nbsp;eRPN:</span>"
			print "</div>"
			print "</a>"
			if [[ ${graphdata[$key:level]} == "o" ]] then
				print "<div style=\"position: absolute; right: 0px; top: 0px;\"class=\"statbtn\" onclick='togglestatmenu(\"${graphdata[$key:objname]}\",\"$FDATE\",\"$LDATE\");'><i class=\"fa fa-line-chart\" title=\"Statistics for ${graphdata[$key:display]}\"></i></div>"
				#print "<div style=\"position: absolute; left: 0px; top: 0px; font-size: ${SMALLNODEFONTSIZE}px;\">E:<br>R:</div>"
			fi
			if [[ $VGgroupStats == "true" && ${graphdata[$key:level]} == "g" ]] then
				obj=${graphdata[$key:objname]}
		                bldg=${obj/'-'*/}
                		ast=${obj/*'-'/}
		                typ=${ast:0:3}
                	        rest=${ast:3:100}
		                rest=${rest/n/g}

                	        groupobj="$bldg-$typ$rest"

				print "<div style=\"position: absolute; right: 0px; top: 0px;\"class=\"statbtn\" onclick='togglestatmenu(\"$groupobj\",\"$FDATE\",\"$LDATE\");'><i class=\"fa fa-line-chart\" title=\"Group statistics for ${graphdata[$key:display]}\"></i></div>"
			fi
			if [[ ${graphdata[$key:level]} == "o" ]] then
				print "<div style=\"position: absolute; right: 0px; top: ${ALARMBTNOFFSET}px;\"class=\"statbtn\" onclick='togglealarmmenu(\"${graphdata[$key:objname]}\",\"$FDATE\",\"$LDATE\");'><i class=\"fa fa-bell-o\" title=\"Alarms for ${graphdata[$key:display]}\"></i></div>"
			fi
			print "</div>"
		;;
	esac
done

##this completes the last graph
print "<!-- end div $CURGRAPH -->"
print "</div>"
gheight=${graphdata[$CURGRAPH:height]}
(( canvasOffset = gheight * -1 ))
canvasOffset=$canvasOffset"px"
print "<div id=\"graphvizSplines_$CURGRAPH\" style=\"margin-left: auto; margin-right: auto; width: ${graphdata[$CURGRAPH:width]}px; height: ${graphdata[$CURGRAPH:height]}px; margin-top: $canvasOffset;\">"
print "<canvas id=\"canvas_$CURGRAPH\" width=\"${graphdata[$CURGRAPH:width]}px\" height=\"${graphdata[$CURGRAPH:height]}px\">Error: Canvas not supported</canvas>"
print "</div>"

print "<div id='splinedata' style='visibility:hidden; width: 1px; height: 1px;'>"
for key in ${!unqlines[@]}; do
	kl=${key/*':'/}
	kr=${key/':'*/}
	print "2|#067ab4|${linedata[$key:sx]},${linedata[$key:sy]}|${linedata[$key:exy]}|${graphdata[$kl:objname]}|${graphdata[$kr:objname]}|canvas_${linedata[$key:parent]}"
done
print "</div>"
print "<div id='edgedata' style='visibility:hidden;'>"
ifs=$IFS
IFS='|'
ddspp $datadir/iedge.dds | while read a b c d e f g h; do
	[[ $f == "" ]] && continue
	print "$d|$f"
done
IFS=$ifs
print "</div>"

[[ $VERBOSE -ge 1 ]] && print "<pre>"
[[ $VERBOSE -ge 1 ]] && cat $datadir/debug.log
[[ $VERBOSE -ge 1 ]] && print "</pre>"
