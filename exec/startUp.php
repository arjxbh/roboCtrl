#!/usr/bin/php
<?php
require_once('./head.php');

// this should be run once on startup
// it clears all the queues, starts all the queues
// and then triggers the startup animation

// define all the queue types
// TODO: move this out to a configuration file
log::write('info', 'starting up...');
$queueTypes = array('talk', 'walk', 'move', 'express', 'system');

// clear any stops that may exist in the queue
$roboQueue = new queue($QUEUE_PATH, $MODEL_PATH);
$roboQueue->eraseQueue('stop');

// clear each individual queue type then
// launch each individual queue type consumer
foreach($queueTypes as $type){
  $roboQueue = new queue($QUEUE_PATH, $MODEL_PATH);
  $roboQueue->eraseQueue($type);
  //exec( $BASE_DIR.'/exec/consume.php '.$type.' > /dev/null 2>'.$LOG_PATH.'/error.log &');
}

exec($BASE_DIR.'/exec/watcher.sh '.$QUEUE_PATH.' '.$BASE_DIR.' '.$LOG_FILE.' > /dev/null 2>'.$LOG_PATH.'/error.log &' );

log::write('startup','startup animation should go here');
// inject startup animation into queue
$roboMove = new animate($QUEUE_PATH);
$roboMove->startUp();
log::write('startup','startup animation should be over now');

//launch subconscious
exec( $BASE_DIR.'/exec/subconscious.php > /dev/null 2>&1 &');

log::write('info', 'start up complete');
