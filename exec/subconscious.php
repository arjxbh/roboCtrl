#!/usr/bin/php
<?php

require_once('head.php');

//subconscious producer should always be running as long
//as the robot is on

// TODO: log startup time
// set random blink interval (minutes)
$startupTime = robot::getMicrotime()/1000;
$blinkTime = $startupTime;
$blinkInterval = rand(5,20);

log::write('info','starting subconscious');

// time of no action before robot issues sleep / shutdown (minutes)
$sleepTime = 30;

// time of no action before robot issues bored action (minutes)
$boredTime = 20;

while(true){
  $currentTime = robot::getMicrotime()/1000;

  if( ( $currentTime - $blinkTime ) >= ( $blinkInterval ) ){
    $blink = new express($QUEUE_PATH);
    $blink->enqueue('face','blink');
    $blinkTime = $currentTime;
    $blinkInterval = rand(5,20);
  }

  // this code is probably flawed. THe robot will never get bored
  // or fall asleep because he is always entertained by his blinking
  $latestQueue = exec($BASE_DIR.'/exec/readDir.sh READONLY '.$QUEUE_PATH.' '.$BASE_DIR.' '.$LOG_FILE );
  $queueParts = explode('.',$latestQueue);
  if( ( $currentTime - $queueParts[0] ) >= ( $sleepTime * 60 ) ){
    $robotAction = new animate($QUEUE_PATH);
    //$robotAction->goToSleep();
  }elseif( ( $currentTime - $queueParts[0] ) >= ( $boredTime * 60 ) ){
    $robotAction = new animate($QUEUE_PATH);
    $robotAction->getBored();
  }

  // one second sleep time used because none of these
  // actions are really critical
  sleep(1);
}
