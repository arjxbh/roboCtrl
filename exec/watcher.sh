#!/bin/ksh

#exec 2>/dev/null

#exec 2> $0.log.$$
#PS4='$LINENO $SECONDS: '
#set -x

queueDir=$1
execDir="$2/exec"
logFile=$3

queueType[0]='talk'
queueType[1]='walk'
queueType[2]='move'
queueType[3]='express'
queueType[4]='system'

robotStopped=0

while [[ true ]]; do
  numberOfQueuesRunning=0

  for queueName in ${queueType[@]}; do
    numberOfProcesses=`ps -Ao pid,args -ww | grep consume | grep $queueName | grep -v grep | wc -l`
    numberOfProcesses=${numberOfProcesses//' '/}

    #echo "`date` | $queueName - $numberOfProcesses" >> $logFile

    ## if the consumer isn't running, restart it
    if [[ $numberOfProcesses -lt 1 ]]; then
	echo "launching thread for: $queueName" >> $logFile      
	`${execDir}/consume.php ${queueName} > /dev/null 2>>${logFile} &`
    else
	(( numberOfQueuesRunning++ ))
	robotStopped=0
    fi
  done

  if [[ $numberOfQueuesRunning -eq 0 ]]; then
	echo 'stopped robot detected for 1 cycle' >> $logFile
    (( robotStopped++ ))
  fi

  if [[ $robotStopped -ge 2 ]]; then
	echo 'fixing stopped condition' >> $logFile
    `rm ${queueDir}/*.q`
    `rm ${queueDir}/*.p`
	robotStopped=0
  fi

  sleep 5
done
