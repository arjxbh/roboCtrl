<?php
require_once('../exec/head.php');
?>

<html>
	<head>
		<title>
			Robot Control System
		</title>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
		<!-- font awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, user-scalable=no"/>
		<script type='text/javascript'>
	    // STRAIGHT should be in meters, but it is not reliable
			const STRAIGHT = 1;
			// TURN should be in degrees, but this is not reliable
			const TURN = 30;
			// SPIN should be in degrees, but due to surface variation
			// it is not reliable. SPIN is used for a quick, drastic turn
			const SPIN = 180;
			// REPEAT_INTERVAL is in ms
			var REPEAT_INTERVAL = 500;
			// MOVE should be in PWM
			const MOVE_PWM = 50;

			//moveIterate function calls the move API
			//to iterate the movement
			function moveIterate(joint, direction){
				if(window.navigator.vibrate){
					window.navigator.vibrate(200);
				}
				$.get( '/roboAPI/move.php?action=' + joint + '&arg=' + direction*MOVE_PWM , function(data){
					Materialize.toast(data, 500);
				})
			}

			//moveScripted calls the move API to run a
			//scripted action
			function moveScripted(action){
				if(window.navigator.vibrate){
					window.navigator.vibrate(200);
				}
				$.get( '/roboAPI/move.php?action=' + action + '&arg=none', function(data){
					Materialize.toast(data, 500);
				})
			}

			//talk
			function speak(action, arg){
				if(window.navigator.vibrate){
					window.navigator.vibrate(200);
				}
				$.get( '/roboAPI/talk.php?action=' + action + '&arg=' + arg, function(data){
					Materialize.toast(data, 500);
				})
			}

      // stop function calls the stop API
			function stop(){
				$.get( '/roboAPI/stop.php', function(data){
					Materialize.toast(data, 500);
				})
			}

      function callAPI(action){
				var apiEndPoint = 'none';
				var apiArgs = 'none';

				var actionParts = action.split('.');

				switch(actionParts[0]){
					case 'runForward':
						apiEndPoint = 'walk';
						apiArgs = '?action=straight&arg='+STRAIGHT*2;
						REPEAT_INTERVAL = 10000;
						break;
					case 'walkForward':
						apiEndPoint = 'walk';
						apiArgs = '?action=straight&arg='+STRAIGHT;
						REPEAT_INTERVAL = 5000;
						break;
					case 'walkBackward':
						apiEndPoint = 'walk';
						apiArgs = '?action=straight&arg='+STRAIGHT*-1;
						REPEAT_INTERVAL = 5000;
						break;
					case 'walkRight':
						apiEndPoint = 'walk';
						apiArgs = '?action=spin&arg='+TURN*-1;
						REPEAT_INTERVAL = 5000;
						break;
					case 'walkLeft':
						apiEndPoint = 'walk';
						apiArgs = '?action=spin&arg='+TURN*1;
						REPEAT_INTERVAL = 5000;
						break;
					case 'spinLeft':
						apiEndPoint = 'walk';
						apiArgs = '?action=spin&arg='+SPIN*1;
						REPEAT_INTERVAL = 10000;
						break;
					case 'spinRight':
						apiEndPoint = 'walk';
						apiArgs = '?action=spin&arg='+SPIN*-1;
						REPEAT_INTERVAL = 10000;
						break;
					case 'move':
						moveIterate(actionParts[1],actionParts[2]);
						REPEAT_INTERVAL = 500;
						break;
				}

				if( apiEndPoint != 'none' ){
					$.get( '/roboAPI/'+apiEndPoint+'.php'+apiArgs , function(data){
						Materialize.toast(data, 500);
					});
				}
			}

			var actionTimer;

			function touchEvent(action){
				if(window.navigator.vibrate){
					window.navigator.vibrate(300);
				}
				switch(action){
					case 'cancel':
						clearTimeout(actionTimer);
						break;
					default:
						callAPI(action);
						// actionTimer = setTimeout(function(){
						// 	touchEvent(action);
						// }, REPEAT_INTERVAL);
						break;
				}
			}

			function findAllButtons(){
				$('.btn').each(function(i, obj) {
					obj.addEventListener('touchstart', function(){
						touchEvent(obj.value);
					});
					obj.addEventListener('touchend', function(){
						touchEvent('cancel');
					});
					obj.addEventListener('touchleave', function(){
						touchEvent('cancel');
					});
					obj.addEventListener('touchcancel', function(){
						touchEvent('cancel');
					});
				});
			}
		</script>
	</head>
	<body>
	<div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s3"><a class='active' href="#basic">Voice</a></li>
        <li class="tab col s2"><a href="#ceremony">Ceremony</a></li>
        <li class="tab col s2"><a href="#party">Party</a></li>
        <li class="tab col s2"><a href="#advanced">Advanced</a></li>
      </ul>
    </div>
	</div>
	<div class='row'>
    <div id="basic" class="col s12">
			<div class='row'>
				<button class='btn waves-effect waves-light' onclick="speak('speak','hello my name is henry')">
					Name Henry
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','what is your name')">
					What's yours?
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','hello sophia')">
					Hello Sophia
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','I am strictly programmed to follow the laws of robotics')">
					Follow Laws
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','turn down for what')">
					Turn down
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','does anyone know the time?')">
					Time?
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','When it is time to party, we will party hard!')">
					Party Hard
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','did you ever hear the one about the robot? nevermind the joke ran out of juice.')">
					Bad Joke
				</button>
			</div>
			<div class='row'>
				<input type='text' id='speechText'><button class='btn waves-effect waves-light' onclick="speak('speak',document.getElementById('speechText').value)">Talk</button>
			</div>
			<div class='row'>
				<div class='col s2'>
					<button class='btn waves-effect waves-light' onclick="speak('volume','20%')">
						20%
					</button>
				</div>
				<div class='col s2'>
					<button class='btn waves-effect waves-light' onclick="speak('volume','40%')">
						40%
					</button>
				</div>
				<div class='col s2'>
					<button class='btn waves-effect waves-light' onclick="speak('volume','60%')">
						60%
					</button>
				</div>
				<div class='col s2'>
					<button class='btn waves-effect waves-light' onclick="speak('volume','80%')">
						80%
					</button>
				</div>
				<div class='col s2'>
					<button class='btn waves-effect waves-light' onclick="speak('volume','90%')">
						90%
					</button>
				</div>
				<div class='col s2'>
					<button class='btn waves-effect waves-light' onclick="speak('volume','100%')">
						100%
					</button>
				</div>
			</div>
			<div class='row'>
				Pickup Lines
			</div>
			<div class='row'>
				<button class='btn waves-effect waves-light' onclick="speak('speak','Did you sit in a pile of sugar? Cause you have a pretty sweet asymtote.')">
					Pile Sugar
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','Do you know what my shirt is made of? Boyfriend material.')">
					Boyfriend Material
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','Is your daddy a Baker? Because youve got some nice buns!')">
					Nice Buns
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','Do you have a map? Im getting lost in your eyes.')">
					Lost Map
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','I dont have a library card, but do you mind if I check you out?')">
					Library Card
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','Are you a parking ticket? ....Cause youve got fine written all over you.')">
					Parking Ticket
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','Are those mirrors on your pants? Because I definitely see myself in them.')">
					Pant Mirrors
				</button>
				<button class='btn waves-effect waves-light' onclick="speak('speak','If I could rearrange the alphabet Id put you and I together.')">
					Alphabet
				</button>
			</div>

		</div>

		<div id="ceremony" class="col s12">
			<div class='row s12'>
				<div class='col s6'>
					<!-- start walk buttons -->
					<div class='row grey lighten-3 center-align'>
						<div class='col'>
							<button class='btn waves-effect waves-light blue darken-2' value='spinLeft'>
								<i class="fa fa-arrow-left"></i>
							</button>
						</div>
						<div class='col'>
							<button class='btn waves-effect waves-light blue darken-2' value='runForward'>
								<i class="fa fa-arrow-up"></i>
							</button>
						</div>
						<div class='col'>
							<button class='btn waves-effect waves-light blue darken-2' value='spinRight'>
								<i class="fa fa-arrow-right"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkForward'>
								<i class="fa fa-chevron-up"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkLeft'>
								<i class="fa fa-chevron-left"></i>
							</button>
						</div>
						<div class='col s4 center-align'>
							WALK
						</div>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkRight'>
								<i class="fa fa-chevron-right"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkBackward'>
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
					</div>
					<!-- end walk buttons -->
				</div>
				<div class='col s6 right-align'>
					<button class='btn waves-effect waves-light' onclick="moveScripted('lookAround');">
						Look Around
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('reachRings');">
						Reach for Rings
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('grabRings');">
						Grab Rings
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('presentRings');">
						Present Rings
					</button>
					<br><br>
					<button class='btn waves-effect waves-light orange' onclick="moveScripted('releaseRings');">
						Release Rings
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('armsDown');">
						Arms Down
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('centerHead');">
						Center Head
					</button>
				</div>
			</div>
		</div>
    <div id="party" class="col s12">
			<div class='row s12'>
				<div class='col s6'>
					<!-- start walk buttons -->
					<div class='row grey lighten-3'>
						<div class='col'>
							<button class='btn waves-effect waves-light blue darken-2' value='spinLeft'>
								<i class="fa fa-arrow-left"></i>
							</button>
						</div>
						<div class='col'>
							<button class='btn waves-effect waves-light blue darken-2' value='runForward'>
								<i class="fa fa-arrow-up"></i>
							</button>
						</div>
						<div class='col'>
							<button class='btn waves-effect waves-light blue darken-2' value='spinRight'>
								<i class="fa fa-arrow-right"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkForward'>
								<i class="fa fa-chevron-up"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkLeft'>
								<i class="fa fa-chevron-left"></i>
							</button>
						</div>
						<div class='col s4 center-align'>
							WALK
						</div>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkRight'>
								<i class="fa fa-chevron-right"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkBackward'>
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
					</div>
					<!-- end walk buttons -->
				</div>
				<div class='col s3 right-align'>
					<button class='btn waves-effect waves-light white black-text' onclick="moveScripted('clear');">
						Clear
					</button>
					<br><br>
					<button class='btn waves-effect waves-light red' onclick="moveScripted('colorWipeRed');">
						Wipe Red
					</button>
					<br><br>
					<button class='btn waves-effect waves-light blue' onclick="moveScripted('colorWipeBlue');">
						Wipe Blue
					</button>
					<br><br>
					<button class='btn waves-effect waves-light green' onclick="moveScripted('colorWipeGreen');">
						Wipe Green
					</button>
					<br><br>
					<button class='btn waves-effect waves-light pink' onclick="moveScripted('colorWipeFuscia');">
						Wipe Fuschia
					</button>
					<br><br>
					<button class='btn waves-effect waves-light white black-text' onclick="moveScripted('theaterChaseWhite');">
						Chase White
					</button>
					<br><br>
					<button class='btn waves-effect waves-light red' onclick="moveScripted('theaterChaseRed');">
						Chase Red
					</button>
					<br><br>
					<button class='btn waves-effect waves-light blue' onclick="moveScripted('theaterChaseBlue');">
						Chase Blue
					</button>
					<br><br>
					<button class='btn waves-effect waves-light purple' onclick="moveScripted('rainbow');">
						rainbow 1
					</button>
					<br><br>
					<button class='btn waves-effect waves-light purple' onclick="moveScripted('rainbowCycle');">
						rainbow 2
					</button>
					<br><br>
					<button class='btn waves-effect waves-light purple' onclick="moveScripted('theaterChaseRainbow');">
						rainbow 3
					</button>
					<br><br>
				</div>
				<div class='col s3 right-align'>
					<button class='btn waves-effect waves-light' onclick="moveScripted('robotDance');">
						Robot
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('higher');">
						Higher
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('handsUp');">
						Hands Up
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('twist');">
						Twist
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('spinDJ');">
						Spin DJ
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('chop');">
						Chop
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('centerHead');">
						Center Head
					</button>
					<br><br>
					<button class='btn waves-effect waves-light' onclick="moveScripted('armsDown');">
						Arms Down
					</button>
					<br><br>
				</div>
			</div>
		</div>

    <div id="advanced" class="col s12">
			<div class='row s12'>
				<div class='col s6 grey lighten-2'>
					<!-- start walk buttons -->
					<div class='row grey lighten-3 center-align'>
						<div class='col'>
							<button class='btn waves-effect waves-light blue darken-2' value='spinLeft'>
								<i class="fa fa-arrow-left"></i>
							</button>
						</div>
						<div class='col'>
							<button class='btn waves-effect waves-light waves-effect waves-light blue darken-2' value='runForward'>
								<i class="fa fa-arrow-up"></i>
							</button>
						</div>
						<div class='col'>
							<button class='btn waves-effect waves-light waves-effect waves-light blue darken-2' value='spinRight'>
								<i class="fa fa-arrow-right"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light waves-effect waves-light blue' value='walkForward'>
								<i class="fa fa-chevron-up"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkLeft'>
								<i class="fa fa-chevron-left"></i>
							</button>
						</div>
						<div class='col s4 center-align'>
							WALK
						</div>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkRight'>
								<i class="fa fa-chevron-right"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light blue' value='walkBackward'>
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
					</div>
					<!-- end walk buttons -->
				</div>
				<div class='col s6  grey lighten-1'>
					<!-- start head buttons -->
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light blue' value='move.neckTilt.-1'>
								<i class="fa fa-chevron-up"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='move.neckPan.1'>
								<i class="fa fa-chevron-left"></i>
							</button>
						</div>
						<div class='col s4 center-align'>
							HEAD
							<br>
							<button class='btn waves-effect waves-light' onclick="moveScripted('centerHead');">
								Center
							</button>
						</div>
						<div class='col s4 center-align'>
							<button class='btn waves-effect waves-light blue' value='move.neckPan.-1'>
								<i class="fa fa-chevron-right"></i>
							</button>
						</div>
					</div>
					<div class='row'>
						<div class='col s12 center-align'>
							<button class='btn waves-effect waves-light blue' value='move.neckTilt.1'>
								<i class="fa fa-chevron-down"></i>
							</button>
						</div>
					</div>
					<!-- end head buttons -->
				</div>
			</div>
			<!-- end big row 1 -->
			<div class='row'>
				<div class='col s6 center-align'>
					<div class='row'>
						<b>Left Arm</b>
					</div>
					<div class='row'>
						Shoulder Pan
					</div>
					<div class='row'>
						<button class='btn waves-effect waves-light blue' value='move.leftShoulderPan.1'>
							<i class="fa fa-chevron-left"></i>
						</button>
						<button class='btn waves-effect waves-light blue' value='move.leftShoulderPan.-1'>
							<i class="fa fa-chevron-right"></i>
						</button>
					</div>
					<div class='row'>
						Shoulder Tilt
					</div>
					<div class='row'>
						<button class='btn waves-effect waves-light blue' value='move.leftShoulderTilt.1'>
							<i class="fa fa-chevron-left"></i>
						</button>
						<button class='btn waves-effect waves-light blue' value='move.leftShoulderTilt.-1'>
							<i class="fa fa-chevron-right"></i>
						</button>
					</div>
					<div class='row'>
						Elbow
					</div>
					<div class='row'>
						<button class='btn waves-effect waves-light blue' value='move.leftElbow.1'>
							<i class="fa fa-chevron-left"></i>
						</button>
						<button class='btn waves-effect waves-light blue' value='move.leftElbow.-1'>
							<i class="fa fa-chevron-right"></i>
						</button>
					</div>
					<div class='row'>
						Wrist
					</div>
					<div class='row'>
						<button class='btn waves-effect waves-light blue' value='move.leftWrist.1'>
							<i class="fa fa-chevron-left"></i>
						</button>
						<button class='btn waves-effect waves-light blue' value='move.leftWrist.-1'>
							<i class="fa fa-chevron-right"></i>
						</button>
					</div>
					<div class='row'>
						Hand Rotate
					</div>
					<div class='row'>
						<button class='btn waves-effect waves-light blue' value='move.leftHandRotate.1'>
							<i class="fa fa-chevron-left"></i>
						</button>
						<button class='btn waves-effect waves-light blue' value='move.leftHandRotate.-1'>
							<i class="fa fa-chevron-right"></i>
						</button>
					</div>
					<div class='row'>
						Hand
					</div>
					<div class='row'>
						<button class='btn waves-effect waves-light blue' value='move.leftHand.1'>
							<i class="fa fa-chevron-left"></i>
						</button>
						<button class='btn waves-effect waves-light blue' value='move.leftHand.-1'>
							<i class="fa fa-chevron-right"></i>
						</button>
					</div>
				</div>
			<div class='col s6 center-align'>
				<div class='row'>
					<b>Right Arm</b>
				</div>
				<div class='row'>
					Shoulder Pan
				</div>
				<div class='row'>
					<button class='btn waves-effect waves-light blue' value='move.rightShoulderPan.1'>
						<i class="fa fa-chevron-left"></i>
					</button>
					<button class='btn waves-effect waves-light blue' value='move.rightShoulderPan.-1'>
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
				<div class='row'>
					Shoulder Tilt
				</div>
				<div class='row'>
					<button class='btn waves-effect waves-light blue' value='move.rightShoulderTilt.1'>
						<i class="fa fa-chevron-left"></i>
					</button>
					<button class='btn waves-effect waves-light blue' value='move.rightShoulderTilt.-1'>
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
				<div class='row'>
					Elbow
				</div>
				<div class='row'>
					<button class='btn waves-effect waves-light blue' value='move.rightElbow.1'>
						<i class="fa fa-chevron-left"></i>
					</button>
					<button class='btn waves-effect waves-light blue' value='move.rightElbow.-1'>
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
				<div class='row'>
					Wrist
				</div>
				<div class='row'>
					<button class='btn waves-effect waves-light blue' value='move.rightWrist.1'>
						<i class="fa fa-chevron-left"></i>
					</button>
					<button class='btn waves-effect waves-light blue' value='move.rightWrist.-1'>
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
				<div class='row'>
					Hand Rotate
				</div>
				<div class='row'>
					<button class='btn waves-effect waves-light blue' value='move.rightHandRotate.1'>
						<i class="fa fa-chevron-left"></i>
					</button>
					<button class='btn waves-effect waves-light blue' value='move.rightHandRotate.-1'>
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
				<div class='row'>
					Hand
				</div>
				<div class='row'>
					<button class='btn waves-effect waves-light blue' value='move.rightHand.1'>
						<i class="fa fa-chevron-left"></i>
					</button>
					<button class='btn waves-effect waves-light blue' value='move.rightHand.-1'>
						<i class="fa fa-chevron-right"></i>
					</button>
				</div>
			</div>

			</div>
			<div class='row center-align'>
				<button class='btn waves-effect waves-light red white-text center-align' onclick="moveScripted('shutdown');">
					shutdown
				</button>
				<button class='btn waves-effect waves-light red white-text center-align' onclick="moveScripted('reboot');">
					reboot
				</button>
			</div>

		</div>
	</div>

	<!-- start big row 2 -->
	<div class='row s12 center-align red lighten-3'>
		<div class='col s12'>
			<button class='btn waves-effect waves-light red white-text center-align' onclick='stop()'>
				STOP!
			</button>
			<button class='btn waves-effect waves-light red white-text center-align' onclick="moveScripted('startUp');">
				startup
			</button>
			<button class='btn waves-effect waves-light red white-text center-align' onclick="moveScripted('neutralPosition');">
				neutral
			</button>
		</div>
	</div>

		<!--Import jQuery before materialize.js-->
		<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="js/materialize.min.js"></script>
		<script type='text/javascript'>
			findAllButtons();
		</script>
	</body>
</html>
